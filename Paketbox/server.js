//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		04.01.2020
//Autoren:		Marcel Schneegans
//Dateiname:	server.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	(LOKALE) MODULE / PAKETE
//------------------------------------------------------------------------------------------------------------------------------------------------------
var express = require('express');
var socket = require('socket.io');
var url = require('url');
var db_mail = require('../module/datenbank_mailserver');
var db = require('../module/datenbankanbindung');
var status = require('../module/status');
var mqtt = require('../module/mqtt');
var config = require('../konfigurationsdatei.json');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	WEBANWENDUNG
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Erstellt den Server und vergibt den Port 8080
//--------------------------------------------------
var app = express();
var server = app.listen(8080, function(){
    console.log('Server hoert auf den Port 8080...');
});
app.use(express.static('../Paketbox/public'));


//--------------------------------------------------
//Stellt die Seite zum Download des Serverlog zur Verfuegung
//--------------------------------------------------
app.get('/downloadServerlog', function(req, res){
    res.download('../Paketbox/public/aktuellerServerlog.txt', 'Serverlog.txt');
});


//--------------------------------------------------
//Stellt die Seite zum Download der empfangenen Pakete zur Verfuegung
//--------------------------------------------------
app.get('/downloadEmpfangenePakete', function(req, res){
    res.download('../Paketbox/public/aktuelleEmpfangenePakete.txt', 'Pakete.txt');
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	STATISCHE VARIABLEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//Datenbankverbindung
var dbVerbindung = db.verbindeDatenbank();

//Speichert die Versionsnummer
var version = config.version;

//Speichert den Teamnamen
var team = config.teamname;

//Speichert den Websocketserver des Servers
var io = socket(server);





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	WEBSOCKET / EVENTHANDLER
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Stellt die Socketverbindung zum Client her
//--------------------------------------------------
io.on('connection', function(socket){
    console.log('Socketverbindung mit Id: ', socket.id);
    let socketUrl = socket.conn.request.headers.referer; //Speichert die URL des Clients
    console.log(socketUrl);





    //------------------------------------------------------------------------------------------------------------------------------------------------------
    //	WEBOBERFLAECHE - UEBERSICHT/INDEX
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    if(socketUrl.search('8080/') != -1){
        console.log('Index.html');

        //Sendet die Werte des Status
        sendeStatus();

        //Sendet die Listen "Aktuelle Pakete" und "Empfangene Pakete"
        Uebersicht_SendeListeEmpfangenePakete(socket);
        Uebersicht_SendeListeAktuellePakete(socket);
        EmpfangenePakete_SendeListe(socket);
        ErwartetePakete_SendeListe(socket);
    
        //--------------------------------------------------
        //Traegt ein neues Paket in der Datenbank ein
        //--------------------------------------------------
        socket.on('Uebersicht_AktuellesPaketHinzufuegen', function(daten){
            if(daten.sendungsnummer != ""){
                db_mail.eintragen(dbVerbindung, daten.sendungsnummer, daten.notiz);
            }
        });

        //--------------------------------------------------
        //Oeffnet die Paketbox
        //--------------------------------------------------
        socket.on('Uebersicht_BoxOeffnen', function(daten){
            mqtt.oeffneTuere(0);
        });
    }





    //------------------------------------------------------------------------------------------------------------------------------------------------------
    //	WEBOBERFLAECHE - ERWARTETE PAKETE
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    if(socketUrl.search('8080/ErwartetePakete.html') != -1){
        console.log('ErwartetePakete.html');

        //Sendet die Listen "Aktuelle Pakete" und "Empfangene Pakete"
        ErwartetePakete_SendeListe(socket);
        Uebersicht_SendeListeEmpfangenePakete(socket);
        Uebersicht_SendeListeAktuellePakete(socket);
        EmpfangenePakete_SendeListe(socket);

        //--------------------------------------------------
        //Fuegt ein Paket in der Datenbank hinzu
        //--------------------------------------------------
        socket.on('ErwartetePakete_AktuellePaketeHinzufuegen', function(daten){
            if(daten.sendungsnummer != ""){
                db_mail.eintragen(dbVerbindung, daten.sendungsnummer, daten.notiz);
                ErwartetePakete_SendeListe(socket);
                Uebersicht_SendeListeEmpfangenePakete(socket);
                Uebersicht_SendeListeAktuellePakete(socket);
                EmpfangenePakete_SendeListe(socket);
            }
        });

        //--------------------------------------------------
        //Loescht ein Paket aus der Datenbank
        //--------------------------------------------------
        socket.on('ErwartetePakete_ButtonLoeschen', function(daten) {
            db.loescheAktuellePaketeAusDatenbank(dbVerbindung, daten.sendungsnummer);
            setTimeout(() => {
                ErwartetePakete_SendeListe(socket);
                Uebersicht_SendeListeEmpfangenePakete(socket);
                Uebersicht_SendeListeAktuellePakete(socket);
                EmpfangenePakete_SendeListe(socket);
            }, 500);
        });

        //--------------------------------------------------
        //Aendert ein Paket in der Datenbank
        //--------------------------------------------------
        socket.on('ErwartetePakete_ButtonAendern', function(daten) {
            db.aendereAktuellePaketeAusDatenbank(dbVerbindung, daten.alteSendungsnummer, daten.geaenderteSendungsnummer, daten.geaenderteNotiz);
            setTimeout(() => {
                ErwartetePakete_SendeListe(socket);
                Uebersicht_SendeListeEmpfangenePakete(socket);
                Uebersicht_SendeListeAktuellePakete(socket);
                EmpfangenePakete_SendeListe(socket);
            }, 500);
        });

        //--------------------------------------------------
        //Sendet die Liste mit Sortierung "Aufsteigend"
        //--------------------------------------------------
        socket.on('ErwartetePakete_SortierungASC', function(daten){
            db.leseAktuellePaketeAusDatenbank(dbVerbindung, 0, 'ASC', '', function(rueckgabe){
                for(let i = 0; i < rueckgabe.length; i++){
                    let eingetragen = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_eingetragen));
                    io.sockets.emit('ErwartetePakete_AktuellePakete', {
                        id: i,
                        anzahlPakete: rueckgabe.length,
                        sendungsnummer: rueckgabe[i].Sendungsnummer,
                        notiz: rueckgabe[i].Notiz,
                        eingetragen: eingetragen
                    });
                }
            });
        });

        //--------------------------------------------------
        //Sendet die Liste mit der Sortierung "Absteigend"
        //--------------------------------------------------
        socket.on('ErwartetePakete_SortierungDESC', function(daten){
            db.leseAktuellePaketeAusDatenbank(dbVerbindung, 0, 'DESC', '', function(rueckgabe){
                for(let i = 0; i < rueckgabe.length; i++){
                    let eingetragen = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_eingetragen));
                    io.sockets.emit('ErwartetePakete_AktuellePakete', {
                        id: i,
                        anzahlPakete: rueckgabe.length,
                        sendungsnummer: rueckgabe[i].Sendungsnummer,
                        notiz: rueckgabe[i].Notiz,
                        eingetragen: eingetragen
                    });
                }
            });
        });
    }





    //------------------------------------------------------------------------------------------------------------------------------------------------------
    //	WEBOBERFLAECHE - EMPFANGENE PAKETE
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    if(socketUrl.search('8080/EmpfangenePakete.html') != -1){
        console.log('EmpfangenePakete.html');

        //Sendet die Liste an den Client
        EmpfangenePakete_SendeListe(socket);

        //--------------------------------------------------
        //Loescht die aktuelle Liste aus der Datenbank
        //--------------------------------------------------
        socket.on('EmpfangenePakete_ListeLoeschen', function(daten){
            db.loescheEmpfangenePaketeAusDatenbank(dbVerbindung, 1, "");
            io.sockets.emit('EmpfangenePakete_ListeLeeren', {});
        });

        //--------------------------------------------------
        //Laedt die aktuelle Liste herunter
        //--------------------------------------------------
        socket.on('EmpfangenePakete_ListeErstellen', function(daten){
            let rueckgabe = db.erstelleListeAusEmpfangenePakete(dbVerbindung, 'aktuelleEmpfangenePakete.txt');
            socket.emit('EmpfangenePakete_ListeHerunterladen', {});
        });
        
        //--------------------------------------------------
        //Sendet eine Liste mit Sortierung "Aufsteigend"
        //--------------------------------------------------
        socket.on('EmpfangenePakete_SortierungASC', function(daten){
            db.leseEmpfangenePaketeAusDatenbank(dbVerbindung, 0, 'ASC', function(rueckgabe){
                for(let i = 0; i < rueckgabe.length; i++){
                    let eingetragen = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_eingetragen));
                    let zugestellt = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_zugestellt));
                    socket.emit('EmpfangenePakete_Liste', {
                        id: i,
                        anzahlPakete: rueckgabe.length,
                        sendungsnummer: rueckgabe[i].Sendungsnummer,
                        notiz: rueckgabe[i].Notiz,
                        eingetragen: eingetragen,
                        zugestellt: zugestellt
                    });
                }
            });
        });

        //--------------------------------------------------
        //Sendet eine Liste mit Sortierung "Absteigend"
        //--------------------------------------------------
        socket.on('EmpfangenePakete_SortierungDESC', function(daten){
            db.leseEmpfangenePaketeAusDatenbank(dbVerbindung, 0, 'DESC', function(rueckgabe){
                for(let i = 0; i < rueckgabe.length; i++){
                    let eingetragen = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_eingetragen));
                    let zugestellt = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_zugestellt));
                    socket.emit('EmpfangenePakete_Liste', {
                        id: i,
                        anzahlPakete: rueckgabe.length,
                        sendungsnummer: rueckgabe[i].Sendungsnummer,
                        notiz: rueckgabe[i].Notiz,
                        eingetragen: eingetragen,
                        zugestellt: zugestellt
                    });
                }
            });
        });
    }





    //------------------------------------------------------------------------------------------------------------------------------------------------------
    //	WEBOBERFLAECHE - STATUS
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    if(socketUrl.search('8080/Status.html') != -1){
        console.log('Status.html');

        //Ruft die aktuellen Statuswerte ab
        sendeStatus();
        
        //--------------------------------------------------
        //Leitet die Statusaenderungen ein beim Druecken des Starttasters
        //--------------------------------------------------
        socket.on('Status_StartTaster', function(daten){
            mqtt.starttasterGedrueckt();
            io.sockets.emit('Status_EingaengeStartTaster', {
                statusStartTaster: "green"
            });
            setTimeout(() => {
                io.sockets.emit('Status_EingaengeStartTaster', {
                    statusStartTaster: "gray"
                });
            }, 1000);
            setTimeout(() => {
                ErwartetePakete_SendeListe(socket);
                Uebersicht_SendeListeEmpfangenePakete(socket);
                Uebersicht_SendeListeAktuellePakete(socket);
                EmpfangenePakete_SendeListe(socket);
            }, 6000);
        });

        //--------------------------------------------------
        //Leitet die Statusaenderungen ein beim Druecken des Fehlertasters
        //--------------------------------------------------
        socket.on('Status_FehlerTaster', function(daten){
            mqtt.fehlertasterGedrueckt();
            io.sockets.emit('Status_EingaengeFehlerTaster', {
                statusFehlerTaster: "red"
            });
            setTimeout(() => {
                io.sockets.emit('Status_EingaengeFehlerTaster', {
                    statusFehlerTaster: "gray"
                });
            }, 1000);
        });

        //--------------------------------------------------
        //Leitet die Statusaenderungen ein beim Druecken des Quittierentasters
        //--------------------------------------------------
        socket.on('Status_QuittierenTaster', function(daten){
            mqtt.fehlerQuittieren();
        });
    }





    //------------------------------------------------------------------------------------------------------------------------------------------------------
    //	WEBOBERFLAECHE - EREIGNISSE
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    if(socketUrl.search('8080/Ereignisse.html') != -1){
        console.log('Ereignisse.html');

        //--------------------------------------------------
        //Sendet die gesamte Liste des Serverlogs
        //--------------------------------------------------
        db.leseServerlogAusDatenbank(dbVerbindung, 'DESC', 'A', function(rueckgabe){
            for(let i = 0; i < rueckgabe.length; i++){
                let zeit = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit));
                socket.emit('Ereignisse_Liste', {
                    zeit: zeit,
                    code: rueckgabe[i].Code,
                    text: rueckgabe[i].Text,
                });
            }
        });

        //--------------------------------------------------
        //Loescht den Serverlog aus der Datenbank
        //--------------------------------------------------
        socket.on('Ereignisse_ListeLoeschen', function(daten){
            db.loescheServerlogAusDatenbank(dbVerbindung);
            io.sockets.emit('Ereignisse_ListeLeeren', {});
        });

        //--------------------------------------------------
        //Sendet den Serverlog gefiltert nach der Kategorie "Pakete"
        //--------------------------------------------------
        socket.on('Ereignisse_KategoriePakete', function(daten){
            db.leseServerlogAusDatenbank(dbVerbindung, 'DESC', 'P', function(rueckgabe){
                for(let i = 0; i < rueckgabe.length; i++){
                    let zeit = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit));
                    socket.emit('Ereignisse_Liste', {
                        zeit: zeit,
                        code: rueckgabe[i].Code,
                        text: rueckgabe[i].Text,
                    });
                }
            });
        });

        //--------------------------------------------------
        //Sendet den Serverlog gefiltert nach der Kategorie "Einstellungen"
        //--------------------------------------------------
        socket.on('Ereignisse_KategorieEinstellungen', function(daten){
            db.leseServerlogAusDatenbank(dbVerbindung, 'DESC', 'E', function(rueckgabe){
                for(let i = 0; i < rueckgabe.length; i++){
                    let zeit = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit));
                    socket.emit('Ereignisse_Liste', {
                        zeit: zeit,
                        code: rueckgabe[i].Code,
                        text: rueckgabe[i].Text,
                    });
                }
            });
        });

        //--------------------------------------------------
        //Sendet den Serverlog gefiltert nach der Kategorie "Fehler"
        //--------------------------------------------------
        socket.on('Ereignisse_KategorieFehler', function(daten){
            db.leseServerlogAusDatenbank(dbVerbindung, 'DESC', 'F', function(rueckgabe){
                for(let i = 0; i < rueckgabe.length; i++){
                    let zeit = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit));
                    socket.emit('Ereignisse_Liste', {
                        zeit: zeit,
                        code: rueckgabe[i].Code,
                        text: rueckgabe[i].Text,
                    });
                }
            });
        });

        //--------------------------------------------------
        //Sendet den Serverlog gefiltert nach der Kategorie "Benachrichtigungen"
        //--------------------------------------------------
        socket.on('Ereignisse_KategorieBenachrichtigungen', function(daten){
            db.leseServerlogAusDatenbank(dbVerbindung, 'DESC', 'B', function(rueckgabe){
                for(let i = 0; i < rueckgabe.length; i++){
                    let zeit = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit));
                    socket.emit('Ereignisse_Liste', {
                        zeit: zeit,
                        code: rueckgabe[i].Code,
                        text: rueckgabe[i].Text,
                    });
                }
            });
        });

        //--------------------------------------------------
        //Sendet den Serverlog gefiltert nach der Kategorie "Alle"
        //--------------------------------------------------
        socket.on('Ereignisse_KategorieAlle', function(daten){
            db.leseServerlogAusDatenbank(dbVerbindung, 'DESC', 'A', function(rueckgabe){
                for(let i = 0; i < rueckgabe.length; i++){
                    let zeit = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit));
                    socket.emit('Ereignisse_Liste', {
                        zeit: zeit,
                        code: rueckgabe[i].Code,
                        text: rueckgabe[i].Text,
                    });
                }
            });
        });

        //--------------------------------------------------
        //Laedt die aktuelle Liste herunter
        //--------------------------------------------------
        socket.on('Ereignisse_ListeErstellen', function(daten){
            let rueckgabe = db.erstelleListeAusServerlog_AktuelleListe(dbVerbindung, 'aktuellerServerlog.txt', daten.kategorie);
            socket.emit('Ereignisse_ListeHerunterladen', {});
        });
    }





    //------------------------------------------------------------------------------------------------------------------------------------------------------
    //	WEBOBERFLAECHE - EINSTELLUNGEN
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    if(socketUrl.search('8080/Einstellungen.html') != -1){
        console.log('Einstellungen.html');

        //--------------------------------------------------
        //Sendet die Mailempfaenger an den Client bei Seitenaufruf
        //--------------------------------------------------
        db.leseMailempfaengerAusDatenbank(dbVerbindung, 0, '', function(rueckgabe){
            for(let i = 0; i < rueckgabe.length; i++){
                socket.emit('Einstellungen_Mailempfaenger', {
                    id: i,
                    anzahlMailempfaenger: rueckgabe.length,
                    name: rueckgabe[i].Name,
                    emailadresse: rueckgabe[i].Emailadresse,
                });
            }
        });

        //--------------------------------------------------
        //Sendet den Nutzungszeitraumbereich an den Client bei Seitenaufruf
        //--------------------------------------------------
        status.holeNutzungszeitraum(function(checkbox, beginn, ende){
            socket.emit('Einstellungen_NutzungszeitraumEmpfangen', {
                nutzungszeitCheckbox: checkbox,
                nutzungszeitBeginn: beginn,
                nutzungszeitEnde: ende
            });
        });

        //--------------------------------------------------
        //Aendert die Nutzungszeiten
        //--------------------------------------------------
        socket.on('Einstellungen_NutzungszeitBestaetigen', function(daten){
            status.setzeNutzungszeitraum(daten.nutzungszeitBeginn, daten.nutzungszeitEnde);
            io.sockets.emit('Einstellungen_NutzungszeitEinstellen', {
                nutzungszeitBeginn: daten.nutzungszeitBeginn,
                nutzungszeitEnde: daten.nutzungszeitEnde
            });
        });

        //--------------------------------------------------
        //Aendert die Aktiviaet des Nutzungszeitraumbereichs
        //--------------------------------------------------
        socket.on('Einstellungen_NutzungszeitCheckbox', function(daten){
            status.setzeNutzungszeitCheckbox(daten.nutzungszeitCheckbox);
            io.sockets.emit('Einstellungen_NutzungszeitCheckboxEinstellen', {
                nutzungszeitCheckbox: daten.nutzungszeitCheckbox
            });
        });

        //--------------------------------------------------
        //Aendert einen Mailempfaenger und sendet neue Liste
        //--------------------------------------------------
        socket.on('Einstellungen_ButtonAendern', function(daten) {
            db.aendereMailempfaengerAusDatenbank(dbVerbindung, daten.alteEmailadresse, daten.geaenderterName, daten.geaenderteEmailadresse);
            setTimeout(() => {
                db.leseMailempfaengerAusDatenbank(dbVerbindung, 0, '', function(rueckgabe){
                    for(let i = 0; i < rueckgabe.length; i++){
                        io.sockets.emit('Einstellungen_Mailempfaenger', {
                            id: i,
                            anzahlMailempfaenger: rueckgabe.length,
                            name: rueckgabe[i].Name,
                            emailadresse: rueckgabe[i].Emailadresse,
                        });
                    }
                });
            }, 500);
        });

        //--------------------------------------------------
        //Fuegt einen neuen Mailempfaenger hinzu und sendet neue Liste
        //--------------------------------------------------
        socket.on('Einstellungen_ButtonHinzufuegen', function(daten) {
            db.schreibeMailempfaengerInDatenbank(dbVerbindung, daten.name, daten.emailadresse);
            setTimeout(() => {
                db.leseMailempfaengerAusDatenbank(dbVerbindung, 0, '', function(rueckgabe){
                    for(let i = 0; i < rueckgabe.length; i++){
                        io.sockets.emit('Einstellungen_Mailempfaenger', {
                            id: i,
                            anzahlMailempfaenger: rueckgabe.length,
                            name: rueckgabe[i].Name,
                            emailadresse: rueckgabe[i].Emailadresse,
                        });
                    }
                });
            }, 500);
        });

        //--------------------------------------------------
        //Loescht einen Mailempfaenger und sendet neue Liste
        //--------------------------------------------------
        socket.on('Einstellungen_ButtonLoeschen', function(daten) {
            db.loescheMailempfaengerAusDatenbank(dbVerbindung, "'" + daten.emailadresse + "'");
            setTimeout(() => {
                db.leseMailempfaengerAusDatenbank(dbVerbindung, 0, '', function(rueckgabe){
                    for(let i = 0; i < rueckgabe.length; i++){
                        socket.emit('Einstellungen_Mailempfaenger', {
                            id: i,
                            anzahlMailempfaenger: rueckgabe.length,
                            name: rueckgabe[i].Name,
                            emailadresse: rueckgabe[i].Emailadresse,
                        });
                    }
                });
            }, 500);
        });
    }





    //------------------------------------------------------------------------------------------------------------------------------------------------------
    //	WEBOBERFLAECHE - FOOTER
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    sendeFooter(socket);
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTION FUERS LISTENABRUF
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Uebersicht_SendeListeEmpfangenePakete
//Sendet an die Uebersichtsseite die Liste der empfangenen Pakete
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Listenaenderung auf Weboberflaeche
//--------------------------------------------------
module.exports.Uebersicht_SendeListeEmpfangenePakete = Uebersicht_SendeListeEmpfangenePakete = function (){
    db.leseEmpfangenePaketeAusDatenbank(dbVerbindung, 1, 'DESC', function(rueckgabe){
        if(rueckgabe.length === 0){
            io.sockets.emit('Uebersicht_EmpfangenePakete', {
                anzahlPakete: rueckgabe.length
            });
        }
        else{
            for(let i = 0; i < rueckgabe.length; i++){
                let zugestellt = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_zugestellt));
                io.sockets.emit('Uebersicht_EmpfangenePakete', {
                    id: i,
                    anzahlPakete: rueckgabe.length,
                    sendungsnummer: rueckgabe[i].Sendungsnummer,
                    zugestellt: zugestellt
                });
            }
        }
        
    });
};

//--------------------------------------------------
//Uebersicht_SendeListeAktuellePakete
//Sendet an die Uebersichtsseite die Liste der aktuellen Pakete
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Listenaenderung auf Weboberflaeche
//--------------------------------------------------
module.exports.Uebersicht_SendeListeAktuellePakete = Uebersicht_SendeListeAktuellePakete=function (){
    db.leseAktuellePaketeAusDatenbank(dbVerbindung, 5, 'ASC', '', function(rueckgabe){
        if(rueckgabe.length === 0){
            io.sockets.emit('Uebersicht_AktuellePakete', {
                anzahlPakete: rueckgabe.length
            });
        }
        else{
            for(let i = 0; i < rueckgabe.length; i++){
                io.sockets.emit('Uebersicht_AktuellePakete', {
                    id: i,
                    anzahlPakete: rueckgabe.length,
                    sendungsnummer: rueckgabe[i].Sendungsnummer,
                    notiz: rueckgabe[i].Notiz
                });
            }
        }
        
    });
};

//--------------------------------------------------
//ErwartetePakete_SendeListe
//Sendet an die "Erwartete Pakete"-Seite die entsprechende Liste
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Listenaenderung auf Weboberflaeche
//--------------------------------------------------
module.exports.ErwartetePakete_SendeListe=ErwartetePakete_SendeListe=function (){
    //Liste Aktuelle Pakete
    db.leseAktuellePaketeAusDatenbank(dbVerbindung, 0, 'ASC', '', function(rueckgabe){
        if(rueckgabe.length === 0){
            io.sockets.emit('ErwartetePakete_AktuellePakete', {
                anzahlPakete: rueckgabe.length
            });
        }
        else{
            for(let i = 0; i < rueckgabe.length; i++){
                let eingetragen = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_eingetragen));
                io.sockets.emit('ErwartetePakete_AktuellePakete', {
                    id: i,
                    anzahlPakete: rueckgabe.length,
                    sendungsnummer: rueckgabe[i].Sendungsnummer,
                    notiz: rueckgabe[i].Notiz,
                    eingetragen: eingetragen
                });
            }
        }
        
    });
};

//--------------------------------------------------
//EmpfangenePakete_SendeListe
//Sendet an die "Empfangene Pakete"-Seite die entsprechende Liste
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Listenaenderung auf Weboberflaeche
//--------------------------------------------------
module.exports.EmpfangenePakete_SendeListe=EmpfangenePakete_SendeListe=function EmpfangenePakete_SendeListe(){
    //Liste Empfangene Pakete
    db.leseEmpfangenePaketeAusDatenbank(dbVerbindung, 0, 'DESC', function(rueckgabe){
        if(rueckgabe.length === 0){
            io.sockets.emit('EmpfangenePakete_Liste', {
                anzahlPakete: rueckgabe.length
            });
        }
        else{
            for(let i = 0; i < rueckgabe.length; i++){
                let eingetragen = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_eingetragen));
                let zugestellt = konvertiereZeitausgabe(new Date(rueckgabe[i].Zeit_zugestellt));
                io.sockets.emit('EmpfangenePakete_Liste', {
                    id: i,
                    anzahlPakete: rueckgabe.length,
                    sendungsnummer: rueckgabe[i].Sendungsnummer,
                    notiz: rueckgabe[i].Notiz,
                    eingetragen: eingetragen,
                    zugestellt: zugestellt
                });
            }
        }
    });
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTION FUER FOOTER
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//sendeFooter
//Sendet die Informatioen des Footers an den Client
//Parameter:
//		pSocket - Clientverbindung
//		pScanner - Wert des Scanners
//Rueckgabewert:
//		(indirekt) - Textaenderung auf Weboberflaeche
//--------------------------------------------------
function sendeFooter(pSocket){
    let datum = konvertiereZeitausgabe(new Date(), 1);
    pSocket.emit('Footer', {
        datum: datum,
        team: team,
        version: version
    });
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTION ZUM STATUS
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//neueStatusVerfuegbar
//Empfaengt die Statuswerte und sendet diese an die Seiten
//Parameter:
//		pBereitschaft - Wert der Bereitschaft
//		pFuellstand - Wert des Fuellstands
//		pFehler - Wert des Fehlers
//		pScanner - Wert des Scanners
//Rueckgabewert:
//		(indirekt) - Statusaenderungen auf Weboberflaeche
//--------------------------------------------------
module.exports.neueStatusVerfuegbar = neueStatusVerfuegbar = function (pBereitschaft, pFuellstand, pFehler, pScanner){
    
    let bereitschaft = "";
    let fuellstand = "";
    let fehler = "";
    let scanner = "";

    //pBereitschaft
    if(pBereitschaft === 1){
        bereitschaft = "green";
    }
    else{
        bereitschaft = "red";
    }
    
    //pFuellstand
    if(pFuellstand === 1){
        fuellstand = "red";
    }
    else{
        fuellstand = "gray";
    }
    
    //pFehler
    if(pFehler === 1){
        fehler = "red";
    }
    else{
        fehler = "green";
    }
    
    //pScanner
    if(pScanner === 1){
        scanner = "green";
    }
    else{
        scanner = "gray";
    }

    
    io.sockets.emit('Status_Statusaenderung', {
        statusBereitschaft: bereitschaft,
        statusFuellstand: fuellstand,
        statusFehler: fehler,
        statusScanner: scanner
    });
};

//--------------------------------------------------
//aktualisiereAusgaengeTueroeffner
//Empfaengt die Zustandswerte des Tueroeffners und sendet diesen an die Seiten
//Parameter:
//		pTueroffner - Wert des Tueroeffners
//Rueckgabewert:
//		(indirekt) - Zustandsaenderung auf Weboberflaeche
//--------------------------------------------------
module.exports.aktualisiereAusgaengeTueroeffner = aktualisiereAusgaengeTueroeffner = function(pTueroffner){
    if(pTueroffner === 1){
       io.sockets.emit('Status_AusgaengeTueroeffner', {
           statusTueroeffner: "green"
       });
    }
    else{
        io.sockets.emit('Status_AusgaengeTueroeffner', {
            statusTueroeffner: "gray"
        });
    }
};

//--------------------------------------------------
//aktualisiereAusgaengeGrueneLed
//Empfaengt die Zustandswerte der gruenen Led und sendet diesen an die Seiten
//Parameter:
//		pGrueneLed - Wert des Tueroeffners
//Rueckgabewert:
//		(indirekt) - Zustandsaenderung auf Weboberflaeche
//--------------------------------------------------
module.exports.aktualisiereAusgaengeGrueneLed = aktualisiereAusgaengeGrueneLed = function(pGrueneLed){
    if(pGrueneLed === 2){ // blinken
        io.sockets.emit('Status_AusgaengeGrueneLed', {
            statusGrueneLed: "yellow"
        });
        // setTimeout(() => {
        //     io.sockets.emit('Status_AusgaengeGrueneLed', {
        //         statusGrueneLed: "gray"
        //     });
        // }, 1000);
    }
    else if(pGrueneLed === 1){ //an
        io.sockets.emit('Status_AusgaengeGrueneLed', {
            statusGrueneLed: "green"
        });
    }
    else{ //aus
        io.sockets.emit('Status_AusgaengeGrueneLed', {
            statusGrueneLed: "gray"
        });
    }
};

//--------------------------------------------------
//aktualisiereAusgaengeRoteLed
//Empfaengt die Zustandswerte der roten Led und sendet diesen an die Seiten
//Parameter:
//		pRoteLed - Wert des Tueroeffners
//Rueckgabewert:
//		(indirekt) - Zustandsaenderung auf Weboberflaeche
//--------------------------------------------------
module.exports.aktualisiereAusgaengeRoteLed = aktualisiereAusgaengeRoteLed = function(pRoteLed){
    if(pRoteLed === 2){ // 3x blinken
        io.sockets.emit('Status_AusgaengeRoteLed', {
            statusRoteLed: "red"
        });
        setTimeout(() => {
            io.sockets.emit('Status_AusgaengeRoteLed', {
                statusRoteLed: "gray"
            });
        }, 1000);
    }
    else if(pRoteLed === 1){
        io.sockets.emit('Status_AusgaengeRoteLed', {
            statusRoteLed: "red"
        });
    }
    else{
        io.sockets.emit('Status_AusgaengeRoteLed', {
            statusRoteLed: "gray"
        });
    }
};

//--------------------------------------------------
//sendeStatus
//Ruft in einem dauerhaften Interval die Status- und Zustandswerte des Moduls ab
//Parameter:
//		keinen
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function sendeStatus(){
    status.sendeStatus();
    status.sendeAusgaenge();
    setInterval(() => {
        status.sendeStatus();
        status.sendeAusgaenge();
    }, 100);
};

//--------------------------------------------------
//konvertiereZeitausgabe
//Konvertiert ein Datumsobjekt einen entsprechend konvertierten Text
//Parameter:
//		pDateObjekt - Datumsobjekt
//      mode
//          (1) ohne Uhrzeit
//          (0) mit Uhrzeit [Standard]
//Rueckgabewert:
//		konvertierter Text
//--------------------------------------------------
function konvertiereZeitausgabe(pDateObjekt, mode=0) {
    //Speichert die Monatszahlen
    let monate = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    //Speichert die Tageszahlen
    let tage = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

    //Speichert die Stundenzahlen
    let stunden = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];

    //Speichert die Minutenzahlen
    let minuten = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59'];

    if(mode === 1){
        return tage[pDateObjekt.getDate()] + "." + monate[pDateObjekt.getMonth()] + "." + pDateObjekt.getFullYear();
    }
    else{
        return tage[pDateObjekt.getDate()] + "." + monate[pDateObjekt.getMonth()] + "." + pDateObjekt.getFullYear() + " " + stunden[pDateObjekt.getHours()] + ":" + minuten[pDateObjekt.getMinutes()] + " Uhr";
    }
};

//--------------------------------------------------
//holeSocket
//Uebergibt das Socketobjekt
//Parameter:
//		keinen
//Rueckgabewert:
//		Socketobjekt
//--------------------------------------------------
module.exports.holeSocket = holeSocket = function(){
    return io;
};