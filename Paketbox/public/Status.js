//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		08.01.2020
//Autoren:		Marcel Schneegans
//Dateiname:	Einstellungen.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	WEBSOCKET
//------------------------------------------------------------------------------------------------------------------------------------------------------
var socket = io();





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	HTMLELEMENTE
//------------------------------------------------------------------------------------------------------------------------------------------------------

//Button
var button_StartTaster = document.getElementById('button_StartTaster');
var button_FehlerTaster = document.getElementById('button_FehlerTaster');
var button_QuittierenTaster = document.getElementById('button_QuittierenTaster');

//Eingaenge
var i_StartTaster = document.getElementById('i_StartTaster');
var i_FehlerTaster = document.getElementById('i_FehlerTaster');

//Ausgaenge
var i_Tueroeffner = document.getElementById('i_Tueroeffner');
var i_GrueneLed = document.getElementById('i_GrueneLed');
var i_RoteLed = document.getElementById('i_RoteLed');

//Statusanzeigen
var i_Bereitschaft = document.getElementById('i_Bereitschaft');
var i_Fuellstand = document.getElementById('i_Fuellstand');
var i_Fehler = document.getElementById('i_Fehler');
var i_Scanner = document.getElementById('i_Scanner');

//Footer
var p_Datum = document.getElementById('p_Datum');
var p_TeamVersion = document.getElementById('p_TeamVersion');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - ALLGEMEINE INFOS
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert die Inhalte im Footer
//--------------------------------------------------
socket.on('Footer', function(daten){
    p_Datum.innerText = daten.datum;
    p_TeamVersion.innerText = daten.team + " - " + daten.version;
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - BUTTONS
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Sendet, dass der Starttaster gedrueckt wurde
//--------------------------------------------------
button_StartTaster.addEventListener('click', function(){
    socket.emit('Status_StartTaster', {});
});

//--------------------------------------------------
//Sendet, dass der Fehlertaster gedrueckt wurde
//--------------------------------------------------
button_FehlerTaster.addEventListener('click', function(){
    socket.emit('Status_FehlerTaster', {});
});

//--------------------------------------------------
//Sendet, dass der Quittierentaster gedrueckt wurde
//--------------------------------------------------
button_QuittierenTaster.addEventListener('click', function(){
    socket.emit('Status_QuittierenTaster', {});
});

//--------------------------------------------------
//Aktualisiert den Zustand des Starttasters
//--------------------------------------------------
socket.on('Status_EingaengeStartTaster', function(daten){
    i_StartTaster.style.color = daten.statusStartTaster;
});

//--------------------------------------------------
//Aktualisiert den Zustand des Fehlertasters
//--------------------------------------------------
socket.on('Status_EingaengeFehlerTaster', function(daten){
    i_FehlerTaster.style.color = daten.statusFehlerTaster;
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - AUSGAENGE
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert den Zustand des Tueroeffners
//--------------------------------------------------
socket.on('Status_AusgaengeTueroeffner', function(daten){
    i_Tueroeffner.style.color = daten.statusTueroeffner;
});

//--------------------------------------------------
//Aktualisiert den Zustand der gruenen Led
//--------------------------------------------------
socket.on('Status_AusgaengeGrueneLed', function(daten){
    if(daten.statusGrueneLed === "blinken"){
        for(var i = 0; i < 3; i++){
            i_GrueneLed.style.color = "green";
            setTimeout(() => {
                i_GrueneLed.style.color = "gray";
            }, 500);
        }
    }
    else{
        i_GrueneLed.style.color = daten.statusGrueneLed;
    }
});

//--------------------------------------------------
//Aktualisiert den Zustand der roten Led
//--------------------------------------------------
socket.on('Status_AusgaengeRoteLed', function(daten){
    if(daten.statusRoteLed === "blinken"){
        for(var i = 0; i < 3; i++){
            i_RoteLed.style.color = "red";
            setTimeout(() => {
                i_RoteLed.style.color = "gray";
            }, 500);
        }
    }
    else{
        i_RoteLed.style.color = daten.statusRoteLed;
    }
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - STATUS
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert die Werte der Zustaende
//--------------------------------------------------
socket.on('Status_Statusaenderung', function(daten){
    i_Bereitschaft.style.color = daten.statusBereitschaft;
    i_Fuellstand.style.color = daten.statusFuellstand;
    i_Fehler.style.color = daten.statusFehler;
    i_Scanner.style.color = daten.statusScanner;
});