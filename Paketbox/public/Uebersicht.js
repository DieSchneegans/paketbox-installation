//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		05.01.2020
//Autoren:		Marcel Schneegans
//Dateiname:	Uebersicht.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	WEBSOCKET
//------------------------------------------------------------------------------------------------------------------------------------------------------
var socket = io();





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	HTMLELEMENTE
//------------------------------------------------------------------------------------------------------------------------------------------------------

//Listen
var tbody_EmpfangenePakete = document.getElementById('tbody_EmpfangenePakete');
var tbody_AktuellePakete = document.getElementById('tbody_AktuellePakete');

//Neues Paket hinzufuegen
var input_Sendungsnummer = document.getElementById('input_Sendungsnummer');
var input_Notiz = document.getElementById('input_Notiz');
var button_Hinzufuegen = document.getElementById('button_Index_Hinzufuegen');

//Statusinformationen
var button_BoxOeffnen = document.getElementById('button_BoxOeffnen');
var i_Bereitschaft = document.getElementById('i_Bereitschaft');
var i_Fuellstand = document.getElementById('i_Fuellstand');

//Footer
var p_Datum = document.getElementById('p_Datum');
var p_TeamVersion = document.getElementById('p_TeamVersion');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	STATISCHE VARIABLEN
//------------------------------------------------------------------------------------------------------------------------------------------------------
var hilfsvariableFuerHTMLEmpfangenePakete = '';
var hilfsvariableFuerHTMLAktuellePakete = '';





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - ALLGEMEINE INFOS
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert die Inhalte im Footer
//--------------------------------------------------
socket.on('Footer', function(daten){
    p_Datum.innerText = daten.datum;
    p_TeamVersion.innerText = daten.team + " - " + daten.version;
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - LISTENINTERAKTION
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert die Inhalte der Liste "Erwartete Pakete"
//--------------------------------------------------
socket.on('Uebersicht_AktuellePakete', function(daten){
    if(daten.anzahlPakete === 0){
        tbody_AktuellePakete.innerHTML = '';
    }
    else{
        if(daten.id === 0){
            hilfsvariableFuerHTMLAktuellePakete = '';
        }
    
        hilfsvariableFuerHTMLAktuellePakete += "<tr><td>" + daten.sendungsnummer + "</td><td>" + daten.notiz + "</td></tr>";
    
        if(daten.id === daten.anzahlPakete-1){
            tbody_AktuellePakete.innerHTML = hilfsvariableFuerHTMLAktuellePakete;
        }
    }
});

//--------------------------------------------------
//Aktualisiert die Inhalte der Liste "Empfangene Pakete"
//--------------------------------------------------
socket.on('Uebersicht_EmpfangenePakete', function(daten){
    if(daten.anzahlPakete === 0){
        tbody_EmpfangenePakete.innerHTML = '';
    }
    else{
        if(daten.id === 0){
            hilfsvariableFuerHTMLEmpfangenePakete = '';
        }
    
        hilfsvariableFuerHTMLEmpfangenePakete += "<tr><td>" + daten.sendungsnummer + "</td><td>" + daten.zugestellt + "</td></tr>";
    
        if(daten.id === daten.anzahlPakete-1){
            tbody_EmpfangenePakete.innerHTML = hilfsvariableFuerHTMLEmpfangenePakete;
        }
    }
});

//--------------------------------------------------
//Sendet, dass ein neues Paket hinzugefuegt wurde
//--------------------------------------------------
button_Hinzufuegen.addEventListener('click', function(){
    socket.emit('Uebersicht_AktuellesPaketHinzufuegen', {
        sendungsnummer: input_Sendungsnummer.value,
        notiz: input_Notiz.value
    });
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - STATUSINFORMATIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Sendet, dass der Button zum Oeffnen der Box gedrueckt wurde
//--------------------------------------------------
button_BoxOeffnen.addEventListener('click', function(){
    socket.emit('Uebersicht_BoxOeffnen', {});
});

//--------------------------------------------------
//Aktualisiert die Werte der Zustandsanzeigen
//--------------------------------------------------
socket.on('Status_Statusaenderung', function(daten){
    i_Bereitschaft.style.color = daten.statusBereitschaft;
    i_Fuellstand.style.color = daten.statusFuellstand;
});