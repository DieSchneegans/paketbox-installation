//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		07.01.2020
//Autoren:		Marcel Schneegans
//Dateiname:	Ereignisse.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	WEBSOCKET
//------------------------------------------------------------------------------------------------------------------------------------------------------
var socket = io();





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	HTMLELEMENTE
//------------------------------------------------------------------------------------------------------------------------------------------------------

//Serverlog
var tbody_Serverlog = document.getElementById('tbody_Serverlog');
var button_ListeLoeschen = document.getElementById('PopUp_ListeLoeschen_Button');
var button_ListeHerunterladen = document.getElementById('button_ListeHerunterladen');

//Kategorienauswahl
var button_Dropdown = document.getElementById('ereignisseSortieren');
var a_Alle = document.getElementById('a_Alle');
var a_Paket = document.getElementById('a_Paket');
var a_Einstellungen = document.getElementById('a_Einstellungen');
var a_Fehler = document.getElementById('a_Fehler');
var a_Benachrichtigungen = document.getElementById('a_Benachrichtigungen');

//Footer
var p_Datum = document.getElementById('p_Datum');
var p_TeamVersion = document.getElementById('p_TeamVersion');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - ALLGEMEINE INFOS
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert die Inhalte im Footer
//--------------------------------------------------
socket.on('Footer', function(daten){
    p_Datum.innerText = daten.datum;
    p_TeamVersion.innerText = daten.team + " - " + daten.version;
});

//--------------------------------------------------
//Aktualisiert die Liste des Serverlogs
//--------------------------------------------------
socket.on('Ereignisse_Liste', function(daten){
    tbody_Serverlog.innerHTML += "<tr><td>" + daten.zeit + "</td><td>" + daten.code + "</td><td>" + daten.text + "</td></tr>";
});

//--------------------------------------------------
//Leert die angezeigte Liste
//--------------------------------------------------
socket.on('Ereignisse_ListeLeeren', function(daten){
    tbody_Serverlog.innerHTML = "";
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - LISTENINTERAKTION
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Sendet, dass die aktuelle Liste geloescht werden soll
//--------------------------------------------------
button_ListeLoeschen.addEventListener('click', function(){
    socket.emit('Ereignisse_ListeLoeschen', {});
});

//--------------------------------------------------
//Sendet, dass die aktuelle Liste heruntergeladen werden soll
//--------------------------------------------------
button_ListeHerunterladen.addEventListener('click', function(){
    socket.emit('Ereignisse_ListeErstellen', {
        kategorie: button_Dropdown.innerHTML
    });
});

//--------------------------------------------------
//Oeffnet die Seite zum Herunterladen des Serverlogs
//--------------------------------------------------
socket.on('Ereignisse_ListeHerunterladen', function(daten){
    window.open('/downloadServerlog');
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - KATEGORIEAUSWAHL
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Bereitet die Seite vor und sendet, dass der Serverlog mit der Kategorie "Alle" abgerufen werden soll
//--------------------------------------------------
a_Paket.addEventListener('click', function(){
    tbody_Serverlog.innerHTML = "";
    socket.emit('Ereignisse_KategoriePakete', {});
    button_Dropdown.innerHTML = "Pakete";
});

//--------------------------------------------------
//Bereitet die Seite vor und sendet, dass der Serverlog mit der Kategorie "Einstellungen" abgerufen werden soll
//--------------------------------------------------
a_Einstellungen.addEventListener('click', function(){
    tbody_Serverlog.innerHTML = "";
    socket.emit('Ereignisse_KategorieEinstellungen', {});
    button_Dropdown.innerHTML = "Einstellungen";
});

//--------------------------------------------------
//Bereitet die Seite vor und sendet, dass der Serverlog mit der Kategorie "Fehler" abgerufen werden soll
//--------------------------------------------------
a_Fehler.addEventListener('click', function(){
    tbody_Serverlog.innerHTML = "";
    socket.emit('Ereignisse_KategorieFehler', {});
    button_Dropdown.innerHTML = "Fehler";
});

//--------------------------------------------------
//Bereitet die Seite vor und sendet, dass der Serverlog mit der Kategorie "Benachrichtigungen" abgerufen werden soll
//--------------------------------------------------
a_Benachrichtigungen.addEventListener('click', function(){
    tbody_Serverlog.innerHTML = "";
    socket.emit('Ereignisse_KategorieBenachrichtigungen', {});
    button_Dropdown.innerHTML = "Benachrichtigungen";
});

//--------------------------------------------------
//Bereitet die Seite vor und sendet, dass der Serverlog mit der Kategorie "Alle" abgerufen werden soll
//--------------------------------------------------
a_Alle.addEventListener('click', function(){
    tbody_Serverlog.innerHTML = "";
    socket.emit('Ereignisse_KategorieAlle', {});
    button_Dropdown.innerHTML = "Alle";
});