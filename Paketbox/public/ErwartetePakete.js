//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		07.01.2020
//Autoren:		Marcel Schneegans
//Dateiname:	ErwartetePakete.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	WEBSOCKET
//------------------------------------------------------------------------------------------------------------------------------------------------------
var socket = io();





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	HTMLELEMENTE
//------------------------------------------------------------------------------------------------------------------------------------------------------

//Aktuelle Pakete
var tbody_AktuellePakete = document.getElementById('tbody_AktuellePakete');

//Neues Paket
var input_Sendungsnummer = document.getElementById('input_Sendungsnummer');
var input_Notiz = document.getElementById('input_Notiz');
var button_Hinzufuegen = document.getElementById('button_Hinzufuegen');

//Pop-Up Aendern
var input_Aendern_Sendungsnummer = document.getElementById('PopUp_Aendern_Sendungsnummer');
var input_Aendern_Notiz = document.getElementById('PopUp_Aendern_Notiz');
var button_Aendern = document.getElementById('PopUp_Aendern_Button');

//Footer
var p_Datum = document.getElementById('p_Datum');
var p_TeamVersion = document.getElementById('p_TeamVersion');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	STATISCHE VARIABLEN
//------------------------------------------------------------------------------------------------------------------------------------------------------
var hilfsvariableFuerHTML = '';
var sendungsnummer = '';





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - ALLGEMEINE INFOS
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert die Inhalte im Footer
//--------------------------------------------------
socket.on('Footer', function(daten){
    p_Datum.innerText = daten.datum;
    p_TeamVersion.innerText = daten.team + " - " + daten.version;
});

//--------------------------------------------------
//button_Aendern_Geklickt
//Stellt die Pop-Up Initalwerte zur Verfuegung
//Parameter:
//		pSendungsnummer - Sendungsnummer der Tabellenzeile
//		pNotiz - Noitz der Tabellenzeile
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function button_Aendern_Geklickt(pSendungsnummer, pNotiz){
    input_Aendern_Sendungsnummer.value = pSendungsnummer;
    input_Aendern_Notiz.value = pNotiz;
    sendungsnummer = pSendungsnummer;
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - EMPFANGEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert die Liste der aktuellen Pakete
//--------------------------------------------------
socket.on('ErwartetePakete_AktuellePakete', function(daten){
    if(daten.anzahlPakete === 0){
        tbody_AktuellePakete.innerHTML = '';
    }
    else{
        if(daten.id === 0){
            hilfsvariableFuerHTML = '';
        }
    
        hilfsvariableFuerHTML += "<tr><td>" + daten.sendungsnummer + "</td><td>" + daten.notiz + "</td><td>" + daten.eingetragen + "</td><td class='text-center' id='button_tabelle'><button type='button' class='button_tabelle' data-toggle='modal' data-target='#popupAendernPakete' onclick='button_Aendern_Geklickt(" + daten.sendungsnummer + ", &apos;" + daten.notiz + "&apos;)'><i class='fas fa-edit'></i></button> <button type='button' class='button_tabelle' onclick='button_Loeschen_Geklickt(" + daten.sendungsnummer + ")'><i class='fas fa-trash'></i></button></td></tr>";
    
        if(daten.id === daten.anzahlPakete-1 || daten.anzahlPakete === 1){
            tbody_AktuellePakete.innerHTML = hilfsvariableFuerHTML;
        }
    }
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - SENDEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Sendet, dass ein neues Paket hinzugefuegt wird
//--------------------------------------------------
button_Hinzufuegen.addEventListener('click', function(){
    socket.emit('ErwartetePakete_AktuellePaketeHinzufuegen', {
        sendungsnummer: input_Sendungsnummer.value,
        notiz: input_Notiz.value
    });
});

//--------------------------------------------------
//Sendet, dass ein Paket geaendert wird
//--------------------------------------------------
button_Aendern.addEventListener('click', function(){
    socket.emit('ErwartetePakete_ButtonAendern', {
        alteSendungsnummer: sendungsnummer,
        geaenderteSendungsnummer: Number(input_Aendern_Sendungsnummer.value),
        geaenderteNotiz: input_Aendern_Notiz.value
    });
});

//--------------------------------------------------
//button_Loeschen_Geklickt
//Sendet, dass ein Paket geloescht werden soll
//Parameter:
//		pSendungsnummer - Sendungsnummer der Tabellenzeile
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function button_Loeschen_Geklickt(pSendungsnummer){
    socket.emit('ErwartetePakete_ButtonLoeschen', {
        sendungsnummer: pSendungsnummer
    });
};

//--------------------------------------------------
//i_SortierungASC
//Sendet die Anfrage, dass eine aufsteigende Sortierung benoetigt wird
//Parameter:
//		keinen
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function i_SortierungASC(){
    socket.emit('ErwartetePakete_SortierungASC', {});
};

//--------------------------------------------------
//i_SortierungDESC
//Sendet die Anfrage, dass eine absteigende Sortierung benoetigt wird
//Parameter:
//		keinen
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function i_SortierungDESC(){
    socket.emit('ErwartetePakete_SortierungDESC', {});
};

