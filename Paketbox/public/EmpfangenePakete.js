//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		07.01.2020
//Autoren:		Marcel Schneegans
//Dateiname:	EmpfangenePakete.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	WEBSOCKET
//------------------------------------------------------------------------------------------------------------------------------------------------------
var socket = io();





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	HTMLELEMENTE
//------------------------------------------------------------------------------------------------------------------------------------------------------

//Empfangene Pakete
var tbody_EmpfangenePakete = document.getElementById('tbody_EmpfangenePakete');
var button_ListeLoeschen = document.getElementById('PopUp_ListeLoeschen_Button');
var button_ListeHerunterladen = document.getElementById('button_ListeHerunterladen');

//Footer
var p_Datum = document.getElementById('p_Datum');
var p_TeamVersion = document.getElementById('p_TeamVersion');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	STATISCHE VARIABLEN
//------------------------------------------------------------------------------------------------------------------------------------------------------
var hilfsvariableFuerHTML = '';





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - EMPFANGEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert die Inhalte im Footer
//--------------------------------------------------
socket.on('Footer', function(daten){
    p_Datum.innerText = daten.datum;
    p_TeamVersion.innerText = daten.team + " - " + daten.version;
});

//--------------------------------------------------
//Aktualisiert die Liste mit den empfangenen Paketen
//--------------------------------------------------
socket.on('EmpfangenePakete_Liste', function(daten){
    if(daten.anzahlPakete === 0){
        tbody_EmpfangenePakete.innerHTML = '';
    }
    else{
        if(daten.id === 0){
            hilfsvariableFuerHTML = '';
        }
    
        hilfsvariableFuerHTML += "<tr><td>" + daten.sendungsnummer + "</td><td>" + daten.notiz + "</td><td>" + daten.eingetragen + "</td><td>" + daten.zugestellt + "</td></tr>";
        
        if(daten.id === daten.anzahlPakete-1 || daten.anzahlPakete === 1){
            tbody_EmpfangenePakete.innerHTML = hilfsvariableFuerHTML;
        }
    }
});

//--------------------------------------------------
//Leert die gesamte Liste der empfangenen Pakete
//--------------------------------------------------
socket.on('EmpfangenePakete_ListeLeeren', function(daten){
    tbody_EmpfangenePakete.innerHTML = "";
});

//--------------------------------------------------
//Oeffnet die Seite zum Herunterladen der Liste
//--------------------------------------------------
socket.on('EmpfangenePakete_ListeHerunterladen', function(daten){
    window.open('/downloadEmpfangenePakete');
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - SENDEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//i_SortierungASC
//Sendet die Anfrage, dass eine aufsteigende Sortierung benoetigt wird
//Parameter:
//		keinen
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function i_SortierungASC(){
    socket.emit('EmpfangenePakete_SortierungASC', {});
};

//--------------------------------------------------
//i_SortierungDESC
//Sendet die Anfrage, dass eine absteigende Sortierung benoetigt wird
//Parameter:
//		keinen
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function i_SortierungDESC(){
    socket.emit('EmpfangenePakete_SortierungDESC', {});
};

//--------------------------------------------------
//Sendet, dass die aktuelle Liste der empfangenen Pakete geloescht werden soll
//--------------------------------------------------
button_ListeLoeschen.addEventListener('click', function(){
    socket.emit('EmpfangenePakete_ListeLoeschen', {});
});

//--------------------------------------------------
//Sendet, dass die Liste heruntergeladen werden moechte
//--------------------------------------------------
button_ListeHerunterladen.addEventListener('click', function(){
    socket.emit('EmpfangenePakete_ListeErstellen', {});
});