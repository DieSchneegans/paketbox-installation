//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		08.01.2020
//Autoren:		Marcel Schneegans
//Dateiname:	Einstellungen.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	WEBSOCKET
//------------------------------------------------------------------------------------------------------------------------------------------------------
var socket = io();





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	HTMLELEMENTE
//------------------------------------------------------------------------------------------------------------------------------------------------------

//Mailempfaenger
var tbody_Mailempfaenger = document.getElementById('tbody_Mailempfaenger');

//Pop-Up Aendern
var input_Aendern_Benutzername = document.getElementById('PopUp_Aendern_Benutzername');
var input_Aendern_Mailadresse = document.getElementById('PopUp_Aendern_Mail');
var button_Aendern = document.getElementById('PopUp_Aendern_Button');

//Pop-Up Neuer Benutzer
var input_Neu_Benutzername = document.getElementById('PopUp_Neu_Benutzername');
var input_Neu_Mailadresse = document.getElementById('PopUp_Neu_Mail');
var button_Hinzufuegen = document.getElementById('PopUp_Neu_Button');

//Nutzungszeiten
var input_NutzungszeitBeginn = document.getElementById('input_NutzungszeitBeginn');
var input_NutzungszeitEnde = document.getElementById('input_NutzungszeitEnde');
var input_NutzungszeitCheckbox = document.getElementById('input_NutzungszeitCheckbox');

//Footer
var p_Datum = document.getElementById('p_Datum');
var p_TeamVersion = document.getElementById('p_TeamVersion');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	STATISCHE VARIABLEN
//------------------------------------------------------------------------------------------------------------------------------------------------------
var hilfsvariableFuerHTML = '';
var emailadresse = '';





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	ALLGEMEINE FUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//button_Aendern_Geklickt
//Liefert die Pop-Up Initalwerte fuers Aendern
//Parameter:
//		pName - Name der Tabellenzeile
//		pEmailadresse - Emailadresse der Tabellenzeile
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function button_Aendern_Geklickt(pName, pEmailadresse){
    input_Aendern_Benutzername.value = pName;
    input_Aendern_Mailadresse.value = pEmailadresse;
    emailadresse = pEmailadresse;
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - EMPFANGEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Aktualisiert die Inhalte im Footer
//--------------------------------------------------
socket.on('Footer', function(daten){
    p_Datum.innerText = daten.datum;
    p_TeamVersion.innerText = daten.team + " - " + daten.version;
});

//--------------------------------------------------
//Aktualisiert die Liste mit den Mailempfaenger
//--------------------------------------------------
socket.on('Einstellungen_Mailempfaenger', function(daten){
    if(daten.id === 0){
        hilfsvariableFuerHTML = '';
    }

    hilfsvariableFuerHTML += "<tr><td>" + daten.name + "</td><td>" + daten.emailadresse + "</td><td class='text-center' id='button_tabelle'><button type='button' class='button_tabelle' data-toggle='modal' data-target='#popupAendernKontakt' onclick='button_Aendern_Geklickt(&apos;" + daten.name + "&apos;, &apos;" + daten.emailadresse + "&apos;)'><i class='fas fa-edit'></i></button> <button type='button' class='button_tabelle' onclick='button_Loeschen_Geklickt(&apos;" + daten.emailadresse + "&apos;)'><i class='fas fa-trash'></i></button></td></tr>";
    if(daten.id === daten.anzahlMailempfaenger-1 || daten.anzahlMailempfaenger === 1){
        tbody_Mailempfaenger.innerHTML = hilfsvariableFuerHTML;
    }
});

//--------------------------------------------------
//Aktualisiert die Nutzungszeiten
//--------------------------------------------------
socket.on('Einstellungen_NutzungszeitEinstellen', function(daten){
    input_NutzungszeitBeginn.value = daten.nutzungszeitBeginn;
    input_NutzungszeitEnde.value = daten.nutzungszeitEnde;
});

//--------------------------------------------------
//Aktualisiert die Aktivitaet der Nutzungszeit
//--------------------------------------------------
socket.on('Einstellungen_NutzungszeitCheckboxEinstellen', function(daten){
    input_NutzungszeitCheckbox.checked = daten.nutzungszeitCheckbox;
});

//--------------------------------------------------
//Aktualisiert den gesamten Nutzungszeitraum-Bereich
//--------------------------------------------------
socket.on('Einstellungen_NutzungszeitraumEmpfangen', function(daten){
    input_NutzungszeitCheckbox.checked = daten.nutzungszeitCheckbox;
    input_NutzungszeitBeginn.value = daten.nutzungszeitBeginn;
    input_NutzungszeitEnde.value = daten.nutzungszeitEnde;
});





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	EVENTHANDLER - SENDEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Sendet den neuen Mailempfaenger
//--------------------------------------------------
button_Hinzufuegen.addEventListener('click', function(){
    socket.emit('Einstellungen_ButtonHinzufuegen', {
        name: input_Neu_Benutzername.value,
        emailadresse: input_Neu_Mailadresse.value
    });
});

//--------------------------------------------------
//Sendet den zu aenderen Mailempfaenger
//--------------------------------------------------
button_Aendern.addEventListener('click', function(){
    socket.emit('Einstellungen_ButtonAendern', {
        alteEmailadresse: emailadresse,
        geaenderterName: input_Aendern_Benutzername.value,
        geaenderteEmailadresse: input_Aendern_Mailadresse.value
    });
});

//--------------------------------------------------
//button_Loeschen_Geklickt
//Sendet den zu loeschenden Mailempfaenger
//Parameter:
//		pEmailadresse - Emailadresse der Tabellenzeile
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function button_Loeschen_Geklickt(pEmailadresse){
    socket.emit('Einstellungen_ButtonLoeschen', {
        emailadresse: pEmailadresse
    });
};

//--------------------------------------------------
//NutzungszeitCheckbox
//Sendet die Aktivitaet des Nutzungszeit-Bereichs
//Parameter:
//		keinen
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function NutzungszeitCheckbox(){
    if(input_NutzungszeitCheckbox.checked){
        socket.emit('Einstellungen_NutzungszeitCheckbox', {
            nutzungszeitCheckbox: 1
        });
    }
    else{
        socket.emit('Einstellungen_NutzungszeitCheckbox', {
            nutzungszeitCheckbox: 0
        });
    }
};

//--------------------------------------------------
//Sendet die neuen Werte fuer den Nutzungszeitraum
//--------------------------------------------------
button_NutzungsbereitschaftBestaetigen.addEventListener('click', function(){
    socket.emit('Einstellungen_NutzungszeitBestaetigen', {
        nutzungszeitBeginn: input_NutzungszeitBeginn.value,
        nutzungszeitEnde: input_NutzungszeitEnde.value
    });
});