//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		11.12.2019
//Autoren:		Marcel Schneegans
//Dateiname:	mailanbindung.js
//--------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------------------------------------
//	(LOKALE) MODULE / PAKETE
//------------------------------------------------------------------------------------------------------------------------------------------------------
var mail = require('nodemailer');
var config = require('../konfigurationsdatei.json');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	ALLGEMEINE FUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//erstelleMailserver
//Stellt die Verbindung zu einem Mailserver her
//Parameter:
//		keine
//Rueckgabewert:
//		server - Mailserververbindung
//--------------------------------------------------
module.exports.erstelleMailserver = function(){
	return server = mail.createTransport({
		service:	'gmail',
		auth: {
			user:	config.email.username,
			pass:	config.email.passwort
		}
    });
};

//--------------------------------------------------
//sendeMail
//Sendet eine Mail
//Parameter:
//		pServer - Mailserververbindung
//		pEmpfaenger	- Alle Mailempfaenger mit dem einem Komma getrennt
//		pInhaltBetreff - Text, welcher als Betreff gesendet werden soll
//		pInhaltBody - Text, welcher als Inhalt/Body gesendet werden soll (in HTML möglich)
//Rueckgabewert:
//		keine
//--------------------------------------------------
module.exports.sendeMail = function(pServer, pEmpfaenger, pInhaltBetreff, pInhaltBody){
	let mailOptions = {
		from:		'Paketbox <' + config.email.username + '>',
		bcc:		pEmpfaenger,
		subject:	pInhaltBetreff,
		html:		pInhaltBody
	};
	
	pServer.sendMail(mailOptions, (err, info) => {
		if(err){
			//throw err;
			console.log("FEHLER!");
		}
		else{
			console.log('Email gesendet: ' + info.response);
		}
	});
};