//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		10.12.2019
//Autoren:		Marcel Schneegans
//Dateiname:	datenbankanbindung.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	(LOKALE) MODULE / PAKETE
//------------------------------------------------------------------------------------------------------------------------------------------------------
var http = require('http');
var mail = require('./mailanbindung');
var db = require('./datenbankanbindung');
var mailVerbindung = mail.erstelleMailserver();





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	ALLGEMEINE FUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//sendeMailAnMailempfaenger
//Ermittelt die Mailempfaenger und sendet die Mail
//Parameter:
//      pDb - Datenbankverbindung
//		pBetreff - Betreffszeile der Mail
//      pInhalt - Textinhalt der Mail
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.sendeMailAnMailempfaenger = sendeMailAnMailempfaenger = function (pDb, pBetreff, pInhalt){
    db.leseMailempfaengerAusDatenbank(pDb, 0, '', function(a){
        let empfaenger = "";
        let inhalt = '<HTML><HEAD><TITLE>Paketboxinformation</TITLE><META content=text/html;charset=utf-8 http-equiv=content-type></HEAD><BODY style="COLOR: #000000; BACKGROUND-COLOR: #fafad2"><TABLE style="BORDER-TOP: #eeeeee 2px solid; BORDER-RIGHT: #eeeeee 2px solid; BORDER-COLLAPSE: collapse; BORDER-BOTTOM: #eeeeee 2px solid; BORDER-LEFT: #eeeeee 2px solid" width="100%" align=center><TBODY><TR><TD><TABLE style="BORDER-COLLAPSE: collapse"><TBODY><TR><TD><TABLE style="COLOR: #ffffff; BACKGROUND-COLOR: #6c757d"><TBODY><TR><TD style="FONT-SIZE: 18pt; FONT-FAMILY: Arial, Helvetica, sans-serif; PADDING-BOTTOM: 10px; PADDING-TOP: 10px; PADDING-LEFT: 10px; PADDING-RIGHT: 10px" width=1000 align=center>' + pBetreff + '</TD></TR></TBODY></TABLE></TD></TR><TR><TD style="PADDING-BOTTOM: 10px; PADDING-TOP: 10px; PADDING-LEFT: 30px; PADDING-RIGHT: 30px; BACKGROUND-COLOR: #ffffff"><TABLE style="BORDER-COLLAPSE: collapse"><TBODY><TR><TD style="FONT-SIZE: 13pt; FONT-FAMILY: Arial, Helvetica, sans-serif; WIDTH: 940px; TEXT-ALIGN: center">' + pInhalt + '</TD></TR></TBODY></TABLE></TD></TR><TR><TD style="PADDING-BOTTOM: 10px; PADDING-TOP: 10px; PADDING-LEFT: 20px; PADDING-RIGHT: 20px; BACKGROUND-COLOR: #ffffff"><TABLE><TBODY><TR><TD style="FONT-SIZE: 10pt; FONT-FAMILY: Arial, Helvetica, sans-serif; WIDTH: 960px; TEXT-ALIGN: center"><HR>Sollten Sie diese E-Mail nicht weiter erhalten wollen, so entfernen Sie Ihre Mailadresse aus der Weboberfläche Ihrer Paketbox.<BR></TD></TR></TBODY></TABLE></TD></TR><TR><TD><TABLE style="COLOR: #ffffff; BACKGROUND-COLOR: #6c757d"><TBODY><TR><TD style="FONT-SIZE: 10pt; FONT-FAMILY: Arial, Helvetica, sans-serif; WIDTH: 1000px; PADDING-BOTTOM: 10px; TEXT-ALIGN: center; PADDING-TOP: 10px; PADDING-LEFT: 10px; PADDING-RIGHT: 10px">Diese E-Mail wurde von Ihrer Paketbox automatisch erzeugt.</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></BODY></HTML>';
        for(i in a){
            let emailaddr = a[i].Emailadresse;
            
            empfaenger = empfaenger.concat(',', emailaddr);
        }
        mail.sendeMail(mailVerbindung, empfaenger, pBetreff, inhalt);
    });
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTIONEN ZU PAKETAKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//empfangen
//Haendelt das Empfangen eines Paketes
//Parameter:
//		pDb - Datenbankverbindung
//		pSendungsnummer - Sendungsnummer des empfangenen Paketes
//Rueckgabewert:
//		(-1) - Falls Paket nicht in Datenbank
//		( 1) - Falls alles funktioniert hat
//--------------------------------------------------
module.exports.empfangen = function(pDb, pSendungsnummer, callback){
    db.leseAktuellePaketeAusDatenbank(pDb, 1, '', pSendungsnummer, function(paket){
        if(paket.length == 1){
            db.schreibeEmpfangenePaketeInDatenbank(pDb, paket[0].Sendungsnummer, paket[0].Notiz, paket[0].Zeit_eingetragen, new Date());
            db.loescheAktuellePaketeAusDatenbank(pDb, paket[0].Sendungsnummer);
            db.schreibeServerlogInDatenbank(pDb, 'P200');
            
            let betreff = 'Neues Paket empfangen!';
            let text = 'Es wurde ein Paket mit der Sendungsnummer ' + paket[0].Sendungsnummer + ' und der Notiz ' + paket[0].Notiz + ' in Ihrer Paketbox zugestellt. Sie können nun jederzeit Ihr Paket aus der Paketbox abholen, indem Sie die Paketbox über die Weboberfläche manuell öffnen.';
            sendeMailAnMailempfaenger(pDb, betreff, text);

            callback(1);
        }
        else{
            db.schreibeServerlogInDatenbank(pDb, 'F100');
            let betreff = 'Fehlerhafter Zustellversuch!';
            let text = 'Es wurde versucht ein Paket mit der Sendungsnummer ' + pSendungsnummer + ' zuzustellen. Diese Sendungsnummer hatte leider keine Übereinstimmung mit Ihren erwarteten Paketen. Bitte überprüfen Sie dies!';
            sendeMailAnMailempfaenger(pDb, betreff, text);

            callback(-1);
        }
    });
};

//--------------------------------------------------
//eintragen
//Haendelt das Eintragen eines Paketes zum Empfangen
//Parameter:
//		pDb - Datenbankverbindung
//		pSendungsnummer - Sendungsnummer des einzutragenden Paketes
//      pNotiz - Notiz des einzutragenden Paketes
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.eintragen = function(pDb, pSendungsnummer, pNotiz){
    db.leseAktuellePaketeAusDatenbank(pDb, 1, '', pSendungsnummer, function(paket){
        if(paket.length == 0){
            db.schreibeAktuellePaketeInDatenbank(pDb, pSendungsnummer, pNotiz, new Date());
            db.schreibeServerlogInDatenbank(pDb, 'P100');
            let betreff = 'Neues Paket zum Empfangen eingetragen';
            let text = 'Es wurde ein neues Paket zum Empfangen eingetragen. Die Sendungsnummer ist <i>' + pSendungsnummer + '</i>. Ihre persönliche Notiz ist <i>' + pNotiz + '</i>. Für weitere Informationen schauen Sie bitte auf die Weboberfläche Ihrer Paketbox!';
            sendeMailAnMailempfaenger(pDb, betreff, text);
        }
        else{
            db.schreibeServerlogInDatenbank(pDb, 'F300');
            let betreff = 'Fehlerhafte Paketeintragung';
            let text = 'Es wurde versucht ein Paket in die Liste "Erwartete Pakete" einzutragen. Dieser Vorgang hat nicht funktioniert. Probieren Sie es bitte erneut!';
            sendeMailAnMailempfaenger(pDb, betreff, text);
        }
    })
};