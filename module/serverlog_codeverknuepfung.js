//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		10.12.2019
//Autoren:		Marcel Schneegans
//Dateiname:	serverlog_codeverknuepfung.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	ALLGEMEINE FUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//getServerlogtext
//Gibt den passenden Serverlogtext zum entsprechenden
//Fehlercode zurueck
//Parameter:
//		pCode - Code des Serverlogs
//Rueckgabewert:
//		text - Serverlogtext zum code pCode
//--------------------------------------------------
module.exports.getServerlogtext = function(pCode){
	let text = "";
	switch(pCode){
	
	//Fehler
	case "F100":
		text = "Es wurde ein Paket mit nicht zustellbaren Paketnummer gescannt";
		break;
		
	case "F200":
		text = "Es wurde die Fehlertaste an der Paketbox gedrückt";
		break;
		
	case "F300":
		text = "Es wurde versucht ein Paket in die Liste 'Erwartete Pakete' einzutragen";
		break;
		
	case "F400":
		text = "Der Barcode wurde nicht erkannt oder konnte nicht gescannt werden";
		break;
		
	case "F500":
		text = "Während der Befüllungsrouine ist die Zeit zum scannen abgelaufen";
		break;
		
	case "F600":
		text = "Während der Befüllungsrouine wurde der Fehlertaster gedrückt";
		break;
		
	case "F700":
		text = "";
		break;
		
	case "F800":
		text = "";
		break;
		
	case "F900":
		text = "Die Fehler wurden durch den Button auf der Weboberfläche quittiert";
		break;
		
	//Paket
	case "P100":
		text = "Es wurde eine Paket zum Empfangen eingetragen";
		break;
		
	case "P200":
		text = "Ein Paket wurde in die Paketbox gelegt";
		break;
		
	case "P300":
		text = "Es wurden alle Pakete aus der Liste 'Empfangene Pakete' gelöscht";
		break;
		
	case "P400":
		text = "Es wurden das älteste Pakete aus der Liste 'Empfangene Pakete' gelöscht";
		break;
		
	case "P500":
		text = "Es wurden ein Pakete aus der Liste 'Empfangene Pakete' gelöscht";
		break;
		
	case "P600":
		text = "Es wurde ein Paket aus der Liste 'Aktuelle Pakete' gelöscht";
		break;
		
	case "P700":
		text = "Es wurde ein Paket aus der Liste 'Aktuelle Pakete' geändert";
		break;
		
	case "P800":
		text = "";
		break;
		
	case "P900":
		text = "";
		break;
		
	//Einstellungen
	case "E100":
		text = "Es wurde ein Mailempfänger hinzugefügt";
		break;
		
	case "E200":
		text = "Es wurde ein Mailempfänger gelöscht";
		break;
		
	case "E300":
		text = "Es wurde ein Mailempfänger geändert";
		break;
		
	case "E400":
		text = "";
		break;
		
	case "E500":
		text = "";
		break;
		
	case "E600":
		text = "";
		break;
		
	case "E700":
		text = "";
		break;
		
	case "E800":
		text = "";
		break;
		
	case "E900":
		text = "Es wurde die Nutzungsbereitschaft der Box eingestellt";
		break;
		
	//Benachrichtigungen
	case "B100":
		text = "Alle eingestellten Paketempfänger wurden über den Erhalt des Paketes informiert";
		break;
		
	case "B200":
		text = "Es wurde der Serverlog komplett gelöscht. Jedoch wurde eine Sicherheitskopie als Dokument gespeichert";
		break;
		
	case "B300":
		text = "Die Paketbox ist wieder Bereit";
		break;
		
	case "B400":
		text = "";
		break;
		
	case "B500":
		text = "";
		break;
		
	case "B600":
		text = "";
		break;
		
	case "B700":
		text = "";
		break;
		
	case "B800":
		text = "Der Starttaster wurde gedrückt";
		break;
		
	case "B900":
		text = "Die Tür der Paketbox wurde geöffnet";
		break;
	}
	return text;
}