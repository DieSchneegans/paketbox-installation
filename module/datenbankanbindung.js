//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		10.12.2019
//Autoren:		Marcel Schneegans
//Dateiname:	datenbankanbindung.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	(LOKALE) MODULE / PAKETE
//------------------------------------------------------------------------------------------------------------------------------------------------------
var mysql = require('mysql');
var serverlogtext = require('./serverlog_codeverknuepfung');
var mail = require('./mailanbindung');
var fs = require('fs');
var config = require('../konfigurationsdatei.json');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	ALLGEMEINE FUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//pruefeEingabe
//Ueberprueft eine Eingabe auf Signalworte der SQL Sprache
//Parameter:
//		pEingabe - Eingabe einer Benutzerinteraktion
//Rueckgabewert:
//		(-1) - Falls pEingabe Signalworte enthaelt
//		( 1) - Falls pEingabe keine Signalworte enthaelt
//--------------------------------------------------
function pruefeEingabe(pEingabe) {
    if (pEingabe.search(/INSERT/i) != -1 || pEingabe.search(/DROP/i) != -1 || pEingabe.search(/CREATE/i) != -1 || pEingabe.search(/DELETE/i) != -1 || pEingabe.search(/SELECT/i) != -1 || pEingabe.search(/ORDER/i) != -1 || pEingabe.search(/HAVING/i) != -1 || pEingabe.search(/;/i) != -1 || pEingabe.search(/BETWEEN/i) != -1 || pEingabe.search(/JOIN/i) != -1 || pEingabe.search(/UPDATE/i) != -1 || pEingabe.search(/ALTER/i) != -1){
        console.log("Fehlerhafter Eingabe");
        return -1;
    }
    return 1;
};

//--------------------------------------------------
//verbindeDatenbank
//Stellt eine Verbindung zwischen der Applikation und der Datenbank her
//Parameter:
//		keine
//Rueckgabewert:
//		db - Datenbankverbindung
//--------------------------------------------------
module.exports.verbindeDatenbank = function(){
	let db = mysql.createConnection({
		host:		config.datenbank.host,
		user:		config.datenbank.user,
		password:	config.datenbank.password,
		database:	config.datenbank.database
	});
	
	db.connect((err) => {
		if (err){
			throw err;
		}
	});
	return db;
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTIONEN ZUM SCHREIBEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//schreibeMailempfaengerInDatenbank
//Schreibt in die Datenbank den uebergebenen Empfaenger - Jede Mailadresse kann jedoch nur einmal eingefuegt werden
//Parameter:
//		pDb - Datenbankverbindung
//		pName - Name der Person / Feldinhalt Name
//		pEmailaddr - Emailadresse der Person / Feldinhalt Email
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.schreibeMailempfaengerInDatenbank = function(pDb, pName, pEmailaddr){
    if (pruefeEingabe(pName) == 1 && pruefeEingabe(pEmailaddr) == 1){
		leseMailempfaengerAusDatenbank(pDb, 0, "", function(empfaenger){
			let gleichheit_eingefuegt = false;
			for(i in empfaenger){
				if(empfaenger[i].Emailadresse == pEmailaddr){
					gleichheit_eingefuegt = true;
				}
			}
			if(gleichheit_eingefuegt == false){
				let inhalt =  {Name: pName, Emailadresse: pEmailaddr};
				let befehl = 'INSERT INTO Mailempfaenger SET ?';
				pDb.query(befehl, inhalt,(err, result) =>{
					if (err) throw err;
					schreibeServerlogInDatenbank(pDb, "E100");
				});
			}
		});
	}
};

//--------------------------------------------------
//schreibeServerlogInDatenbank
//Schreibt in die Datenbank die Serverlogeintraege
//Parameter:
//		pDb - Datenbankverbindung
//		pCode - Fehlercode
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.schreibeServerlogInDatenbank = schreibeServerlogInDatenbank = function(pDb, pCode){
    if(pruefeEingabe(pCode) == 1){
		let timestamp = new Date();
		let text = serverlogtext.getServerlogtext(pCode);
    	let inhalt = {Code: pCode, Text: text, Zeit: timestamp};
    	let befehl = 'INSERT INTO Serverlog SET ?';
    	pDb.query(befehl, inhalt,(err, result) =>{
    		if (err){
    			throw err;
    		}
    	});
    }
};

//--------------------------------------------------
//schreibeEmpfangenePaketeInDatenbank
//Schreibt in die Datenbank die empfangenen Pakete - jedoch maximal 500 gleichzeitig
//Parameter:
//		pDb - Datenbankverbindung
//		pName - Name der Person / Feldinhalt Name
//		pEmailaddr - Emailadresse der Person / Feldinhalt Email
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.schreibeEmpfangenePaketeInDatenbank = function(pDb, pSendungsnummer, pNotiz, pZeitEingetragen, pZeitZugestellt){
	if(pruefeEingabe(pSendungsnummer.toString()) == 1 && pruefeEingabe(pNotiz) == 1){
		let inhalt = {Sendungsnummer: pSendungsnummer, Notiz: pNotiz, Zeit_eingetragen: pZeitEingetragen, Zeit_zugestellt: pZeitZugestellt};
		let befehl = 'INSERT INTO Empfangene_Pakete SET ?';

		pDb.query('SELECT COUNT(*) AS Anzahl FROM Empfangene_Pakete', (err, anzahl) => {
			if(anzahl[0].Anzahl < 500){
				pDb.query(befehl, inhalt,(err, result) =>{
					if (err){
						throw err;
					}
				}); 
			}
			else{
				loescheEmpfangenePaketeAusDatenbank(pDb, 2, '');
				pDb.query(befehl, inhalt,(err, result) =>{
					if (err){
						throw err;
					}
				}); 
			}
		});
	}
};

//--------------------------------------------------
//schreibeAktuellePaketeInDatenbank
//Schreibt in die Datenbank die aktuelle Pakete
//Parameter:
//		pDb - Datenbankverbindung
//		pSendungsnummer - Sendungsnummer des Paketes
//		pNotiz - Notiz des Paketes
//		pZeitEingetragen - Eintragungszeit des Paketes
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.schreibeAktuellePaketeInDatenbank = function(pDb, pSendungsnummer, pNotiz, pZeitEingetragen){
	console.log('schreibeAktuellePaketeInDatenbank', pSendungsnummer, pNotiz, pZeitEingetragen);
	if(pruefeEingabe(pSendungsnummer.toString()) == 1 && pruefeEingabe(pNotiz) == 1){
    	let inhalt = {Sendungsnummer: pSendungsnummer, Notiz: pNotiz, Zeit_eingetragen: pZeitEingetragen};
    	let befehl = 'INSERT INTO Aktuelle_Pakete SET ?';
    	pDb.query('SELECT COUNT(*) AS Anzahl FROM Aktuelle_Pakete', (err, anzahl) => {
			if(anzahl[0].Anzahl < 500){
				pDb.query(befehl, inhalt,(err, result) =>{
					if (err) throw err;
				}); 
			}
		});
    }
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTIONEN ZUM LESEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//leseMailempfaengerAusDatenbank
//Liest aus der Datenbank die Mailempfaenger aus
//Parameter:
//		pDb - Datenbankverbindung
//		pAnzahl
//			(0) - Es werden alle Eintraege aus der Datenbank gelesen
//			(1) - Es wird der Eintrag mit der Sendungsnummer pSendungsnummer aus der Datenbank gelesen
//		pEmaiaddr - Falls ein gezielter Eintrag gesucht wird, muss die Mailadresse übergeben werden
//Rueckgabewert:
//		callback(empfaenger) - Alle aus der Datenbank
//--------------------------------------------------
module.exports.leseMailempfaengerAusDatenbank = leseMailempfaengerAusDatenbank = function(pDb, pAnzahl, pEmailaddr, callback){
	let befehl = '';
	if(pAnzahl == 0){
		befehl = 'SELECT * FROM Mailempfaenger ORDER BY Name ASC';
	}
	else if(pAnzahl == 1){
		befehl = 'SELECT * FROM Mailempfaenger WHERE Emailadresse=\'' + pEmailaddr + '\'';
	}
	pDb.query(befehl, (err, empfaenger) =>{
        if(err)throw err;
        else callback(empfaenger);
    });
};

//--------------------------------------------------
//leseServerlogAusDatenbank
//Liest aus der Datenbank den aktuellen Serverlog aus und gibt diesen
//aufsteigend oder absteigend zurueck
//Parameter:
//		pDb - Datenbankverbindung
//		pSortierung
//			('ASC') - absteigend sortiert
//			('DESC') - aufsteigend sortiert
//		pKategorie (nur bei Sortierung pSortierung = DESC verfügbar)
//			('A') - Alle Serverlogeintraege aller Kategorien
//			('B') - Alle Serverlogeintraege der Kategorie Benachrichtigungen
//			('E') - Alle Serverlogeintraege der Kategorie Einstellungen
//			('F') - Alle Serverlogeintraege der Kategorie Fehler
//			('P') - Alle Serverlogeintraege der Kategorie Paket
//Rueckgabewert:
//		callback(log) - Funktion mit allen Eintraegen des Logs
//--------------------------------------------------
module.exports.leseServerlogAusDatenbank = leseServerlogAusDatenbank = function(pDb, pSortierung, pKategorie, callback){
	let befehl = '';
	if(pSortierung == 'DESC' && pKategorie == 'A'){
		befehl = 'SELECT * FROM Serverlog ORDER BY Zeit DESC';
	}
	if(pSortierung == 'DESC' && pKategorie == 'B'){
		befehl = 'SELECT * FROM Serverlog WHERE Code LIKE \'B%\' ORDER BY Zeit DESC';
	}
	if(pSortierung == 'DESC' && pKategorie == 'E'){
		befehl = 'SELECT * FROM Serverlog WHERE Code LIKE \'E%\' ORDER BY Zeit DESC';
	}
	if(pSortierung == 'DESC' && pKategorie == 'F'){
		befehl = 'SELECT * FROM Serverlog WHERE Code LIKE \'F%\' ORDER BY Zeit DESC';
	}
	if(pSortierung == 'DESC' && pKategorie == 'P'){
		befehl = 'SELECT * FROM Serverlog WHERE Code LIKE \'P%\' ORDER BY Zeit DESC';
	}
	if(pSortierung == 'ASC'){
		befehl = 'SELECT * FROM Serverlog ORDER BY Zeit ASC';
	}

	pDb.query(befehl, (err, log) =>{
		if (err) throw err;
		else callback(log);
	});
};

//--------------------------------------------------
//leseEmpfangenePaketeAusDatenbank
//Liest aus der Datenbank die aktuellen Pakete aus; enweder 5 oder alle
//Parameter:
//		pDb - Datenbankverbindung
//		pNurFuenf
//			(0) - Es werden alle Eintraege aus der Datenbank gelesen
//			(1) - Es werden nur die ersten 5 Eintraege aus der Datenbank gelesen
//		pSortierung
//			('ASC') - absteigend sortiert
//			('DESC') - aufsteigend sortiert
//Rueckgabewert:
//		callback(empfangenePakete) - Alle bzw. 5 Pakete aus der Datenbank
//--------------------------------------------------
module.exports.leseEmpfangenePaketeAusDatenbank = leseEmpfangenePaketeAusDatenbank = function(pDb, pNurFuenf, pSortierung, callback){
	let befehl ='';
	if(pNurFuenf == 1 && pSortierung == 'DESC'){
		befehl = 'SELECT * FROM Empfangene_Pakete ORDER BY Zeit_zugestellt DESC LIMIT 5';
	}
	else if(pNurFuenf == 1 && pSortierung == 'ASC'){
		befehl = 'SELECT * FROM Empfangene_Pakete ORDER BY Zeit_zugestellt ASC LIMIT 5';
	}
	else if(pNurFuenf == 0 && pSortierung == 'DESC'){
		befehl = 'SELECT * FROM Empfangene_Pakete ORDER BY Zeit_zugestellt DESC';
	}
	else if(pNurFuenf == 0 && pSortierung == 'ASC'){
		befehl = 'SELECT * FROM Empfangene_Pakete ORDER BY Zeit_zugestellt ASC';
	}
	
	pDb.query(befehl, (err, empfangenePakete) =>{
        if(err)throw err;
        else callback(empfangenePakete);
    });
};

//--------------------------------------------------
//leseAktuellePaketeAusDatenbank
//Liest aus der Datenbank die aktuellen Pakete aus; enweder 5 oder alle
//Parameter:
//		pDb - Datenbankverbindung
//		pAnzahl
//			(0) - Es werden alle Eintraege aus der Datenbank gelesen
//			(1) - Es wird der Eintrag mit der Sendungsnummer pSendungsnummer aus der Datenbank gelesen
//			(5) - Es werden nur die ersten 5 Eintraege aus der Datenbank gelesen
//		pSortierung
//			('ASC') - absteigend sortiert
//			('DESC') - aufsteigend sortiert
//		pSendungsnummer - Sendungsnummer, wenn gezielt nach einem Eintrag gesucht wird
//Rueckgabewert:
//		callback(aktuellePakete) - Alle bzw. 5 Pakete aus der Datenbank
//--------------------------------------------------
module.exports.leseAktuellePaketeAusDatenbank = leseAktuellePaketeAusDatenbank = function(pDb, pAnzahl, pSortierung, pSendungsnummer, callback){
	let befehl = '';
	if(pAnzahl == 5 && pSortierung == 'DESC'){
		befehl = 'SELECT * FROM Aktuelle_Pakete ORDER BY Sendungsnummer DESC LIMIT 5';
	}
	else if(pAnzahl == 5 && pSortierung == 'ASC'){
		befehl = 'SELECT * FROM Aktuelle_Pakete ORDER BY Sendungsnummer ASC LIMIT 5';
	}
	else if(pAnzahl == 0 && pSortierung == 'DESC'){
		befehl = 'SELECT * FROM Aktuelle_Pakete ORDER BY Sendungsnummer DESC';
	}
	else if(pAnzahl == 0 && pSortierung == 'ASC'){
		befehl = 'SELECT * FROM Aktuelle_Pakete ORDER BY Sendungsnummer ASC';
	}
	else if(pAnzahl == 1){
		befehl = 'SELECT * FROM Aktuelle_Pakete WHERE Sendungsnummer=' + pSendungsnummer;
	}
	
	pDb.query(befehl, (err, aktuellePakete) =>{
        if(err)throw err;
        else callback(aktuellePakete);
    });
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTIONEN ZUM LOESCHEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//loescheMailempfaengerAusDatenbank
//Loescht aus der Datenbank einen beliebigen Eintrag anhand der Emailadresse
//Parameter:
//		pDb - Datenbankverbindung
//		pEmailaddr - Emailadresse, welche aus der Datenbank geloescht werden soll
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.loescheMailempfaengerAusDatenbank = function(pDb, pEmailaddr){
	if(pruefeEingabe(pEmailaddr) == 1){
		let befehl = 'DELETE FROM Mailempfaenger WHERE Emailadresse=' + pEmailaddr;
		pDb.query(befehl, (err, log) =>{
			if (err) throw err;
			schreibeServerlogInDatenbank(pDb, "E200");
		});
	}
};

//--------------------------------------------------
//loescheServerlogAusDatenbank
//Loescht aus der Datenbank den aktuellen Serverlog vollstaendig
//Parameter:
//		pDb - Datenbankverbindung
//Rueckgabewert:
//		(indirekt) - Erzeugt (falls Datei nicht exisitiert) oder haengt an eine Datei den aktuellen Serverlog an
//--------------------------------------------------
module.exports.loescheServerlogAusDatenbank = function(pDb){
	erstelleListeAusServerlog_Gesamtliste(pDb, 'serverlog.txt');
	pDb.query('DELETE FROM Serverlog', (err, log) =>{
		if (err) throw err;
		schreibeServerlogInDatenbank(pDb, "B200");
    });
};

//--------------------------------------------------
//loescheEmpfangenePaketeAusDatenbank
//Loescht aus der Datenbank die Mailempfaenger nach dem Modus pModus
//Parameter:
//		pDb - Datenbankverbindung
//		pModus
//			(1) - alle Eintraege
//			(2) - den ersten eingestellten Eintrag
//			(3) - einen beliebigen bestimmten Eintrag
//		pSendungsnummer (nur fuer pModus=3 relevant - sonst einfach "") - Sendungsnummer des Eintrages, welcher geloescht werden soll
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.loescheEmpfangenePaketeAusDatenbank = function(pDb, pModus, pSendungsnummer){
	let befehl = "";
	if(pModus == 1){ //alle Eintraege
		erstelleListeAusEmpfangenePakete(pDb, 'aktuelleEmpfangenePakete.txt');
		befehl = 'DELETE FROM Empfangene_Pakete';
		pDb.query(befehl, (err, empfangen) =>{
			if (err) throw err;
			schreibeServerlogInDatenbank(pDb, "P300");
		});
	}

	if(pModus == 2){ //ersten eingestellten Eintrag
		leseEmpfangenePaketeAusDatenbank(pDb, 0, 'ASC', function(pakete){
			let bedingung = pakete[0].Sendungsnummer;
			befehl = 'DELETE FROM Empfangene_Pakete WHERE Sendungsnummer=' + bedingung;
			pDb.query(befehl, (err, empfangen) =>{
				if (err) throw err;
				schreibeServerlogInDatenbank(pDb, "P400");
			});
		});
		
	}

	if(pModus == 3){ //beliebigen bestimmten Eintrag
		if(pruefeEingabe(pSendungsnummer.toString()) == 1){
			let bedingung = pSendungsnummer;
			befehl = 'DELETE FROM Empfangene_Pakete WHERE Sendungsnummer=' + bedingung;
			pDb.query(befehl, (err, empfangen) =>{
				if (err) throw err;
				schreibeServerlogInDatenbank(pDb, "P500");
			});
		}
	}
};

//--------------------------------------------------
//loescheAktuellePaketeAusDatenbank
//Loescht aus der Datenbank das aktuelle Paket mit der Sendungsnummer pSendungsnummer
//Parameter:
//		pDb - Datenbankverbindung
//		pSendungsnummer - Sendungsnummer des zu loeschenden Eintrages
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.loescheAktuellePaketeAusDatenbank = function(pDb, pSendungsnummer){
	if(pruefeEingabe(pSendungsnummer.toString()) == 1){
		let befehl = 'DELETE FROM Aktuelle_Pakete WHERE Sendungsnummer=' + pSendungsnummer;
		pDb.query(befehl, (err, aktuell) =>{
			if (err) throw err;
			schreibeServerlogInDatenbank(pDb, "P600");
		});
	}
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTIONEN ZUM AENDERN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//aendereMailempfaengerAusDatenbank
//Aendert einen Mailempfaenger in der Datenbank
//Parameter:
//		pDb - Datenbankverbindung
//		pEmailaddr - Emailadresse, welche aus der Datenbank geloescht werden soll
//		pGeanderterName - zu aenderer Name
//		pGeanderteEmailaddr - zu aendere Emailadresse
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.aendereMailempfaengerAusDatenbank = function(pDb, pEmailaddr, pGeanderterName, pGeanderteEmailaddr){
	leseMailempfaengerAusDatenbank(pDb, 1, pEmailaddr, function(eintrag){
		let name;
		let emailadresse;
		if(eintrag[0].Name != pGeanderterName && pGeanderterName != ""){
			name = pGeanderterName;
		}
		else{
			name = eintrag[0].Name;
		}
		if(eintrag[0].Emailadresse != pGeanderteEmailaddr && pGeanderteEmailaddr != ""){
			emailadresse = pGeanderteEmailaddr;
		}
		else{
			emailadresse = eintrag[0].Emailadresse;
		}

		let befehl = 'UPDATE Mailempfaenger SET Name=\'' + name + '\', Emailadresse=\'' + emailadresse + '\' WHERE Emailadresse=\'' + pEmailaddr + '\'';

		pDb.query(befehl, (err, aktuell) =>{
			if(err)throw err;
			schreibeServerlogInDatenbank(pDb, "E300");
		});
	});
};

//--------------------------------------------------
//aendereAktuellePaketeAusDatenbank
//Aendert ein Paket der DB-Tabelle "Aktuelle_Pakete"
//Parameter:
//		pDb - Datenbankverbindung
//		pSendungsnummer - Sendungsnummer des zu aenderen Eintrages
//		pGeanderteSendungsnummer - Sendungsnummer, welche geändert werden soll
//		pGeanderteNotiz - Notiz, welche geändert werden soll
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.aendereAktuellePaketeAusDatenbank = function(pDb, pSendungsnummer, pGeanderteSendungsnummer, pGeanderteNotiz){
	leseAktuellePaketeAusDatenbank(pDb, 1, '', pSendungsnummer, function(eintrag){
		let sendungsnummer;
		let notiz;
		if(eintrag[0].Sendungsnummer != pGeanderteSendungsnummer && typeof pGeanderteSendungsnummer == "number"){
			sendungsnummer = pGeanderteSendungsnummer;
		}
		else{
			sendungsnummer = eintrag[0].Sendungsnummer;
		}
		if(eintrag[0].Notiz != pGeanderteNotiz && typeof pGeanderteNotiz == "string"){
			notiz = pGeanderteNotiz;
		}
		else{
			notiz = eintrag[0].Notiz;
		}

		let befehl = 'UPDATE Aktuelle_Pakete SET Sendungsnummer=' + sendungsnummer + ', Notiz=\'' + notiz + '\' WHERE Sendungsnummer=' + pSendungsnummer;

		pDb.query(befehl, (err, aktuell) =>{
			if(err)throw err;
			schreibeServerlogInDatenbank(pDb, "P700");
		});
	});
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTIONEN ZUR DATEIINTERAKTION
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//erstelleListeAusEmpfangenePakete
//Erstellt aus der DB-Tabelle eine Datei
//Parameter:
//		pDb - Datenbankverbindung
//		pDateiname - Dateiname der Datei
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.erstelleListeAusEmpfangenePakete = erstelleListeAusEmpfangenePakete = function(pDb, pDateiname){
	leseEmpfangenePaketeAusDatenbank(pDb, 0, "DESC", function(pakete){
		let pakete_text = "Zustellungszeit | Sendungsnummer - Notiz \n";
		for(i in pakete){
			pakete_text = pakete_text.concat('\n',pakete[i].Zeit_zugestellt + ' | ' + pakete[i].Sendungsnummer + ' - ' + pakete[i].Notiz);
		}

		fs.writeFile('../Paketbox/public/'+pDateiname, pakete_text, function(err){
			if (err) throw err;
		});
	});
};

//--------------------------------------------------
//erstelleListeAusServerlog_Gesamtliste
//Schreibt den aktuellen Serverlog in eine Datei, wo der gesamte jemals angelegte Log drin ist
//Parameter:
//		pDb - Datenbankverbindung
//		pDateiname - Dateiname der Datei
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.erstelleListeAusServerlog_Gesamtliste = erstelleListeAusServerlog_Gesamtliste = function(pDb, pDateiname){
	leseServerlogAusDatenbank(pDb, 'ASC', "", function(serverlog){
		let log_text = "";
		for(i in serverlog){
			log_text = log_text.concat('\n',serverlog[i].Zeit + ' - ' + serverlog[i].Code + ' - ' + serverlog[i].Text);
		}
		fs.appendFile('../Paketbox/public/'+pDateiname, log_text, function(err){
			if (err) throw err;
		});
	});
};

//--------------------------------------------------
//erstelleListeAusServerlog_AktuelleListe
//Erstellt eine Datei, wo der aktuelle Serverlog gespeichert wird
//Parameter:
//		pDb - Datenbankverbindung
//		pDateiname - Dateiname der Datei
//		pKategorie
//			('A') - Alle Serverlogeintraege aller Kategorien
//			('B') - Alle Serverlogeintraege der Kategorie Benachrichtigungen
//			('E') - Alle Serverlogeintraege der Kategorie Einstellungen
//			('F') - Alle Serverlogeintraege der Kategorie Fehler
//			('P') - Alle Serverlogeintraege der Kategorie Paket
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.erstelleListeAusServerlog_AktuelleListe = erstelleListeAusServerlog_AktuelleListe = function(pDb, pDateiname, pKategorie){
	if(pKategorie == 'Alle'){
		leseServerlogAusDatenbank(pDb, 'DESC', "A", function(serverlog){
			erstelleServerlogdatei(serverlog, pDateiname);
		});
	}
	if(pKategorie == 'Benachrichtigungen'){
		leseServerlogAusDatenbank(pDb, 'DESC', "B", function(serverlog){
			erstelleServerlogdatei(serverlog, pDateiname);
		});
	}
	if(pKategorie == 'Einstellungen'){
		leseServerlogAusDatenbank(pDb, 'DESC', "E", function(serverlog){
			erstelleServerlogdatei(serverlog, pDateiname);
		});
	}
	if(pKategorie == 'Fehler'){
		leseServerlogAusDatenbank(pDb, 'DESC', "F", function(serverlog){
			erstelleServerlogdatei(serverlog, pDateiname);
		});
	}
	if(pKategorie == 'Pakete'){
		leseServerlogAusDatenbank(pDb, 'DESC', "P", function(serverlog){
			erstelleServerlogdatei(serverlog, pDateiname);
		});
	}
};

//--------------------------------------------------
//erstelleServerlogdatei
//Hilfsmethode für Funktion "erstelleListeAusServerlog_AktuelleListe"
//Parameter:
//		pServerlog - Serverlog aus DB
//		pDateiname - Dateiname der Datei
//Rueckgabewert:
//		keinen
//--------------------------------------------------
function erstelleServerlogdatei(pServerlog, pDateiname){
	let log_text = "";
	for(i in pServerlog){
		log_text = log_text.concat('\n',pServerlog[i].Zeit + ' - ' + pServerlog[i].Code + ' - ' + pServerlog[i].Text);
	}
	fs.writeFile('../Paketbox/public/'+pDateiname, log_text, function(err){
		if (err) throw err;
	});
};