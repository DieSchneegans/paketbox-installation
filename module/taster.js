//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		02.03.2020
//Autoren:		Felix Fakner
//Dateiname:	taster.js
//--------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------------------------------------
//	(LOKALE) MODULE / PAKETE
//------------------------------------------------------------------------------------------------------------------------------------------------------
var Gpio = require("onoff").Gpio;
var mqtt = require("./mqtt");
var server = require('../Paketbox/server');
var config = require('../konfigurationsdatei.json');

//Für Phase 2: mit Consolenausgaben
//Für Phase 3: mit GPIO-Anbindung

//------------------------------------------------------------------------------------------------------------------------------------------------------
//	STATISCHE VARIABLEN
//------------------------------------------------------------------------------------------------------------------------------------------------------
var grueneLed = 1;
var roteLed = 0;
var gpioEingangStarttaster = new Gpio(config.gpio.starttaster, "in", "rising");
var gpioEingangFehlertaster = new Gpio(config.gpio.fehlertaster, "in", 'rising');
//Speichert den Websocketserver des Servers
var io = server.holeSocket();

//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTIONEN DER TASTER
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Leitet die Statusaenderungen ein beim Druecken des Fehlertasters
//--------------------------------------------------
gpioEingangFehlertaster.watch(function(err, wert) {
  if (err) throw err;
  io.sockets.emit('Status_EingaengeFehlerTaster', {
                statusFehlerTaster: "red"
            });
            setTimeout(() => {
                io.sockets.emit('Status_EingaengeFehlerTaster', {
                    statusFehlerTaster: "gray"
                });
            }, 1000);
  mqtt.fehlertasterGedrueckt();
  console.log("Fehlertaster wurde gedrueckt, deshalb hier eine extrem kurze ausgabe in der Konsole. Wussten Sie eigentlich schon, Wenn Lavalampen im Keller betrieben werden nennt man sie Magmalampe.")
});

//--------------------------------------------------
//Leitet die Statusaenderungen ein beim Druecken des Starttasters
//--------------------------------------------------
gpioEingangStarttaster.watch(function(err, wert) {
  if (err) throw err;
  io.sockets.emit('Status_EingaengeStartTaster', {
                statusStartTaster: "green"
            });
            setTimeout(() => {
                io.sockets.emit('Status_EingaengeStartTaster', {
                    statusStartTaster: "gray"
                });
            }, 1000);
  mqtt.starttasterGedrueckt();
  console.log("Der Starttaster wurde gedrueckt, HALLO? DER STARTTASTER WURDE GEDRUECKT! Koennen sie mich denn nicht hören? MANN MANN MANN WAS SOLL DAS DENN, das bringt mich hier aber wieder auf einhundertundachtzig. und dann wirft man mir wieder voe dass ich ein cholerischer raspberry bin.... ich wär doch so gerne ein strawberry p...")
});
