//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		11.12.2019
//Autoren:		Felix Fakner
//Dateiname:	led.js
//--------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------------------------------------
//	(LOKALE) MODULE / PAKETE
//------------------------------------------------------------------------------------------------------------------------------------------------------
var Gpio = require("onoff").Gpio;
var Status = require("./status.js");
var config = require('../konfigurationsdatei.json');

//Für Phase 2: mit Consolenausgaben
//Für Phase 3: mit GPIO-Anbindung

//------------------------------------------------------------------------------------------------------------------------------------------------------
//	STATISCHE VARIABLEN
//------------------------------------------------------------------------------------------------------------------------------------------------------
var gpioAusgangGrueneLed = new Gpio(config.gpio.grueneLed, "out");
var gpioAusgangRoteLed = new Gpio(config.gpio.roteLed, "out");
var gpioAusgangTueroeffner = new Gpio(config.gpio.tueroeffner, "out");
var grueneLed = 1;
gpioAusgangGrueneLed.writeSync(grueneLed);
var roteLed = 0;
gpioAusgangRoteLed.writeSync(roteLed);
var intervalRot;
var intervalGruen;
var fehlerTasterBlockieren = 0;

//------------------------------------------------------------------------------------------------------------------------------------------------------
//	FUNKTIONEN DER LEDS
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//setzeGrueneLed
//Setzt die gruene Led der Paketbox entsprechend
//Parameter:
//		pZustand
//          (0) - Gruene Led ist aus
//			(1) - Gruene Led ist an
//          (2) - Gruene Led blinkt
//Rueckgabewert:
//		(indirekt) - Ueber Konsole/GPIO
//--------------------------------------------------
module.exports.setzeGrueneLed = setzeGrueneLed = function(pZustand) {
  console.log(pZustand);
  switch (pZustand) {
    //aus
    case 0:
      clearInterval(intervalGruen);
      grueneLed = 0;
      console.log("Gruene LED ist aus");
      gpioAusgangGrueneLed.writeSync(0);

      break;

    //leuchten
    case 1:
      grueneLed = 1;
      console.log("Gruene LED leuchtet");
      gpioAusgangGrueneLed.writeSync(1);
      break;

    //blinken
    case 2:
      grueneLed = 2;
      console.log("Gruene LED blinkt");
      intervalGruen = setInterval(()=>{blinken(gpioAusgangGrueneLed)}, 333);
      break;

    default:
      break;
  }
};

//--------------------------------------------------
//blinken
//Invertiert den Wert des Übergebenen Ausgangs
//Parameter:
//		pGpio
//          Definierter GPIO Ausgang
//Rueckgabewert:
//		(indirekt) - Ueber Konsole/GPIO
//--------------------------------------------------
function blinken(pGpio) {
  if (pGpio.readSync() === 0) {
    pGpio.writeSync(1);
  } else {
    pGpio.writeSync(0);
  }
};

//--------------------------------------------------
//setzeRoteLed
//Setzt die rote Led der Paketbox entsprechend
//Parameter:
//		pZustand
//          (0) - Rote Led ist aus
//			    (1) - Rote Led ist an
//          (2) - Rote Led blinkt dreimal
//Rueckgabewert:
//		(indirekt) - Ueber Konsole/GPIO
//--------------------------------------------------
module.exports.setzeRoteLed = setzeRoteLed = function(pZustand) {

  switch (pZustand) {
    //aus
    case 0:
      roteLed = 0;
      console.log("Rote LED ist aus");
      gpioAusgangRoteLed.writeSync(0);

      break;

    //leuchten
    case 1:
      roteLed = 1;
      console.log("Rote LED leuchtet");
      gpioAusgangRoteLed.writeSync(1);

      break;

    //blinken
    case 2:
      if(fehlerTasterBlockieren === 0){
         fehlerTasterBlockieren =1;
      //LEDs ausschalten
      setzeGrueneLed(0);
      setzeRoteLed(0);
      //Rote LED Blinken lassen
      console.log("Rote LED blinkt drei mal");
      intervalRot = setInterval(()=>{blinken(gpioAusgangRoteLed)}, 333);
      setTimeout(()=>{
        clearInterval(intervalRot); 

        setzeGrueneLed(Status.holeBereitschaft());
        if(Status.holeFuellstand() == 1 || Status.holeStoerung() == 1){
        setzeRoteLed(1);
        };
        fehlerTasterBlockieren = 0;
        
      }, 2000);
      
      
      return true;
      }
      break;

    default:
      break;
  }
};

//--------------------------------------------------
//holeAusgaenge
//Gibt die entsprechenden Led-Zustaende zurueck
//Parameter:
//		keinen
//Rueckgabewert:
//		callback
//          (grueneLed) - Zustand der gruenen Led
//          (roteLed) - Zustand der roten Led
//--------------------------------------------------
module.exports.holeAusgaenge = holeAusgaenge = function(callback) {
  callback(grueneLed, roteLed);
};

//--------------------------------------------------
//oeffneTuere
//Setzt den Zustand des Tueroeffners
//Parameter:
//		pZustand
//          (0) - Tueroeffner ist deaktiviert
//			(1) - Tueroeffner ist aktiviert
//Rueckgabewert:
//		(indirekt) - Ueber Konsole/GPIO
//--------------------------------------------------
module.exports.oeffneTuere = oeffneTuere = function(pZustand) {
  if (pZustand === 1) {
    console.log("Tueroeffner = 1");
    gpioAusgangTueroeffner.writeSync(1);
  } else if (pZustand === 0) {
    console.log("Tueroeffner = 0");
    gpioAusgangTueroeffner.writeSync(0);
  }
};
