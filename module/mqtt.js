//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		10.12.2019
//Autoren:		Felix Fakner
//Dateiname:	mqtt.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	(LOKALE) MODULE / PAKETE
//------------------------------------------------------------------------------------------------------------------------------------------------------
var mqtt = require('mqtt');
var db = require('./datenbankanbindung');
var db_mail = require('./datenbank_mailserver');
var status = require('./status');
var led = require('./led');
var config = require('../konfigurationsdatei.json');
const {exec} = require('child_process');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	STATISCHE VARIABLEN
//------------------------------------------------------------------------------------------------------------------------------------------------------
var client = mqtt.connect(config.mqtt);
var dbVerbindung = db.verbindeDatenbank();




//------------------------------------------------------------------------------------------------------------------------------------------------------
//	MQTT CLIENT
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//Ueberprueft ob eine Verbindung zum MQTT-Broker hergestellt ist und abonniert das Topic "Scans"
//--------------------------------------------------
client.on('connect', function () 
{
    client.subscribe('Scans', function(err){
        if(err){
            console.log("Fehler beim Verbinden mit dem MQTT_Broker");
        }
    });

});

//--------------------------------------------------
//Wenn eine Nachricht ueber den MQTT-Broker gesendet wird, wird diese hier empfangen und verarbeitet
//--------------------------------------------------
client.on('message', function (topic, message) 
{
    if(topic == "Scans"){
        console.log("Es wurde in 'Scans' die Nachricht empfangen", message.toString());
        if(message == "Barcode: ' Zeitfehler '")
        {
            console.log('Zeitfehler!');
            db.schreibeServerlogInDatenbank(dbVerbindung, "F400");
            sendeFehlermail();
            status.setzeScanner(0);
            status.setzeBefuellroutine(0);
        }
        else if (message == "Fehlertaster wurde gedrueckt!"){

            console.log('Fehlertaster fuehrte zum Abbruch!');
            db.schreibeServerlogInDatenbank(dbVerbindung, "F600");
            sendeFehlermail();
            status.setzeScanner(0);
            status.setzeBefuellroutine(0);
        }
        else {
            let sendungsnummer = message.slice(11, message.length -2);
            db_mail.empfangen(dbVerbindung, sendungsnummer, function(rueckgabe){
                console.log(rueckgabe);
                if(rueckgabe === 1){
                    console.log("Paket empfangen");
                    oeffneTuere(1);
                    status.setzeScanner(0);
                    status.setzeBefuellroutine(0);
                    status.aktualisiereListen();
                }
                else{
                    console.log("Paket konnte nicht zugestellt werden");
                    led.setzeRoteLed(2);
                    setTimeout(()=>{
                        led.setzeRoteLed(0);
                       
                    },2050);
                     status.setzeScanner(0);
                        status.setzeBefuellroutine(0);
                }
            });
        }
    }
});

//--------------------------------------------------
//sendeBarcodeAnfrage
//Sendet eine Nachricht über den MQTT-Broker, dass ein Barcode gescannt werden soll
//Parameter:
//		keinen
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.sendeBarcodeAnfrage = sendeBarcodeAnfrage = function(){
    client.publish('Barcode', 'scanneBarcode');
};






//------------------------------------------------------------------------------------------------------------------------------------------------------
//	ALLGEMEINE FUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//sendeFehlermail
//Sendet eine Mail, welche einen nicht-quittierbaren Fehler beinhaltet
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Mail an alle Empfaenger
//--------------------------------------------------
function sendeFehlermail() {
    let betreff = "Fehler während der Befüllungsroutine";
    let text = "Während der Befüllungsroutine ist ein Fehler aufgetreten. Dabei handelt es sich wahrscheinlich nicht um einen problematischen Fehler. Ebenfalls muss dieser auch nicht quittiert werden.";
    db_mail.sendeMailAnMailempfaenger(dbVerbindung, betreff, text);
};

//--------------------------------------------------
//sendeFehlertasterMail
//Sendet eine Mail, welche beim Druecken des Fehlertasters gesendet wird
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Mail an alle Empfaenger
//--------------------------------------------------
function sendeFehlertasterMail() {
    let betreff = "Fehlertaster an Ihrer Paketbox gedrückt";
    let text = "Es wurde der Fehlertaster an Ihrer Paketbox gedrückt. Um die Paketbox wieder lauffähig zu machen, muss der Fehler quittiert werden. Rufen Sie dazu die Weboberfläche Ihrer Paketbox auf und drücken Sie den Button 'Quittieren' auf der Statusseite";
    db_mail.sendeMailAnMailempfaenger(dbVerbindung, betreff, text);
};

function sendePaketentnahmeMail() {
    let betreff = "Ein Paket wurde entnommen";
    let text = "Es wurde ein Paket aus der Paketbox entnommen, indem diese manuell geöffnet wurde.";
    db_mail.sendeMailAnMailempfaenger(dbVerbindung, betreff, text);
};



//------------------------------------------------------------------------------------------------------------------------------------------------------
//	TASTER- UND BUTTONINTERAKTION
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//oeffneTuere
//Setzt die verschiedene Status beim Drucken des Button "Paketbox oeffnen" 
//Parameter:
//		pFuellstand - aktueller Fuellstand der Box
//Rueckgabewert:
//		(indirekt) - Serverlogeintrag, Statusaenderungen
//--------------------------------------------------
module.exports.oeffneTuere = oeffneTuere = function(pFuellstand){
    console.log("Tuere wird geoeffnet");
    if(pFuellstand === 1){
        status.setzeFuellstand(1);
        status.setzeTuer(1);
        exec("mpg123 -q /home/pi/softwareprojekt/Audio/Tuereoeffnen.mp3", (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        });
    }
    else{
        status.setzeTuer(1);
        setTimeout(() => {
            status.setzeFuellstand(0);
            db.schreibeServerlogInDatenbank(dbVerbindung, "B300");
            sendePaketentnahmeMail();
        }, 5000);
    }
    db.schreibeServerlogInDatenbank(dbVerbindung, "B900");
};

//--------------------------------------------------
//fehlertasterGedrueckt
//Setzt die verschiedene Status beim Drucken des Button "Fehler-Taster" oder entsprechenden Tasters 
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Serverlogeintrag, Mail, Statusaenderungen
//--------------------------------------------------
module.exports.fehlertasterGedrueckt = fehlertasterGedrueckt = function(){
    console.log("Der Fehlertaster wurde gedrueckt");
    status.setzeStoerung(1);
    sendeFehlertasterMail();
    db.schreibeServerlogInDatenbank(dbVerbindung, "F200");
    client.publish('Scans', 'Fehlertaster wurde gedrueckt!');
};

//--------------------------------------------------
//starttasterGedrueckt
//Startet die Befuellroutine der Paketbox, wenn diese in einem entsprechenden Zustand ist
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Serverlogeintrag
//--------------------------------------------------
module.exports.starttasterGedrueckt = starttasterGedrueckt = function(){
    console.log("Der Starttaster wurde gedrueckt");
    if(status.holeBereitschaft() === 1){
        if(status.holeFuellstand() === 0){
            if(status.holeStoerung() === 0){
                if(status.holeScanner() === 0){
                    exec("mpg123 -q /home/pi/softwareprojekt/Audio/MarioTube.mp3", (error, stdout, stderr) => {
                        if (error) {
                            console.log(`error: ${error.message}`);
                            return;
                        }
                        if (stderr) {
                            console.log(`stderr: ${stderr}`);
                            return;
                        }
                        console.log(`stdout: ${stdout}`);
                    });
                    status.setzeScanner(1);
                    status.setzeBefuellroutine(1);
                    console.log("das ist eine Konsolenausgabe");
                    db.schreibeServerlogInDatenbank(dbVerbindung, "B800");
                    setTimeout(() => {
                        sendeBarcodeAnfrage();
                        
                    }, 100);
                }
            }
        }
    } 
};

//--------------------------------------------------
//fehlerQuittieren
//Setzt die verschiedene Status beim Drucken des Button "Quittieren" 
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Serverlogeintrag, Statusaenderungen
//--------------------------------------------------
module.exports.fehlerQuittieren = fehlerQuittieren = function(){
    console.log("Der Fehler wurde quittiert");
    status.setzeStoerung(0);
    db.schreibeServerlogInDatenbank(dbVerbindung, "F900");
};