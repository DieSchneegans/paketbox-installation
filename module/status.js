//--------------------------------------------------
//ET18 Team 33 - GFM Packers
//Datum:		04.01.2020
//Autoren:		Felix Fakner
//Dateiname:	status.js
//--------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	(LOKALE) MODULE / PAKETE
//------------------------------------------------------------------------------------------------------------------------------------------------------
var led = require('./led');
var server = require('../Paketbox/server');
var config = require('../konfigurationsdatei.json');





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	STATISCHE VARIABLEN
//------------------------------------------------------------------------------------------------------------------------------------------------------
var datum = new Date();
var bereitschaft = 1;
var fuellstand = 0;
var stoerung = 0;
var scanner = 0;
var tuer = 0;
var befuellroutine = 0;
var nutzungszeitCheckbox = 1;
var nutzungszeitBeginn = 0;
var nutzungszeitEnde = 23;





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	ALLGEMEINE FUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//pruefeArrayGleichheit
//Ueberprueft zwei Array der Groesse 4 auf Gleichheit
//Parameter:
//		pArray1 - erstes Array zur Ueberpruefung
//		pArray2 - zweites Array zur Ueberpruefung
//Rueckgabewert:
//		(true) - Falls beide Array gleich sind
//		(false) - Falls es Unterschiede gibt
//--------------------------------------------------
function pruefeArrayGleichheit(pArray1, pArray2){
    for(let i = 0; i < 4; i++){
        //Da nur eine fest definierte Menge Arrays verglichen wird, wird eine Pruefung auf Laenge ignoriert
        if(pArray1[i] != pArray2[i]){
            return false;
        }
    }
    return true;
};

//--------------------------------------------------
//aktualisiereLeds
//Setzt die verschiedenen Leds der Paketbox
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Ledaenderungen
//--------------------------------------------------
function aktualisiereLeds() {
    let ledArray = [bereitschaft, fuellstand, stoerung, befuellroutine];

    console.log(ledArray);
    if(pruefeArrayGleichheit(ledArray, [1, 0, 0, 1])){
        console.log('F1');
        led.setzeGrueneLed(2);
        led.setzeRoteLed(0);
    }
    else if(pruefeArrayGleichheit(ledArray, [1, 0, 0, 0])){
        console.log('F12');
        led.setzeGrueneLed(1);
        led.setzeRoteLed(0);
    }
    else{
        console.log('F123');
        led.setzeGrueneLed(0);
        led.setzeRoteLed(1);
    }
};

//--------------------------------------------------
//aktualisiereNutzungszeit
//Aendert den Bereitschaftsstatus in Abhaengigkeit der Nutzungszeit
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Statusaenderung
//--------------------------------------------------
function aktualisiereNutzungszeit() {
    if(nutzungszeitCheckbox == 1){
        console.log(nutzungszeitBeginn, "+", nutzungszeitEnde, "+", datum.getHours());
        if(Number(nutzungszeitBeginn) < Number(nutzungszeitEnde)){
            console.log("Fall 1");
            if(nutzungszeitBeginn <= 0+datum.getHours() && 0+datum.getHours() <= nutzungszeitEnde){
                console.log("Fall 1a");
                setzeBereitschaft(1);
            }
            else{
                console.log("Fall 1b");
                setzeBereitschaft(0);
            }
        }
        else if(Number(nutzungszeitBeginn) > Number(nutzungszeitEnde)){
            console.log("Fall 2");
            if((nutzungszeitBeginn <= 0+datum.getHours() && datum.getHours() <= 23) || (datum.getHours() >= 0 && datum.getHours() <= nutzungszeitEnde)){
                console.log("Fall 2a");
                setzeBereitschaft(1);
            }
            else{
                console.log("Fall 2b");
                setzeBereitschaft(0);
            }
        }
        else{
            console.log("Fall 3");
            setzeNutzungszeitCheckbox(0);
        }

        
    }
    else{
        console.log("Fall 4");
        setzeBereitschaft(1);
    }
    sendeStatus();
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	SERVERKOMMUNIKATIONSFUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//sendeStatus
//Sendet an den Server die vier Statusgroessen
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Statuswerte von Bereitschaft, Fuellstand, Stoerung und Scanner
//--------------------------------------------------
module.exports.sendeStatus = sendeStatus = function(){
    server.neueStatusVerfuegbar(bereitschaft, fuellstand, stoerung, scanner);
};

//--------------------------------------------------
//sendeAusgaenge
//Sendet an den Server die drei Ausgangsgroessen
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Zustandswerte von Tuer, gruener Led und roter Led
//--------------------------------------------------
module.exports.sendeAusgaenge = sendeAusgaenge = function(){
    led.holeAusgaenge(function(grueneLed, roteLed){
        server.aktualisiereAusgaengeTueroeffner(tuer);
        server.aktualisiereAusgaengeGrueneLed(grueneLed);
        server.aktualisiereAusgaengeRoteLed(roteLed);
    });
};

//--------------------------------------------------
//aktualisiereListen
//Sendet an den Server die Aufforderung die Listen zu aktualisieren
//Parameter:
//		keinen
//Rueckgabewert:
//		keinen
//--------------------------------------------------
module.exports.aktualisiereListen = aktualisiereListen = function(){
    server.ErwartetePakete_SendeListe();
                server.Uebersicht_SendeListeEmpfangenePakete();
                server.Uebersicht_SendeListeAktuellePakete();
                server.EmpfangenePakete_SendeListe();
};



//------------------------------------------------------------------------------------------------------------------------------------------------------
//	SETZEFUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//setzeBereitschaft
//Setzt den Status "Bereitschaft"
//Parameter:
//		pBereitschaft
//          (0) - nicht bereit
//          (1) - bereit
//Rueckgabewert:
//		(indirekt) - Ledaenderung
//--------------------------------------------------
module.exports.setzeBereitschaft = setzeBereitschaft = function(pBereitschaft){
    bereitschaft = pBereitschaft;
    aktualisiereLeds();
};

//--------------------------------------------------
//setzeFuellstand
//Setzt den Status "Fuellstand"
//Parameter:
//		pFuellstand
//          (0) - leer
//          (1) - voll
//Rueckgabewert:
//		(indirekt) - Ledaenderung
//--------------------------------------------------
module.exports.setzeFuellstand = setzeFuellstand = function(pFuellstand){
    fuellstand = pFuellstand;
    aktualisiereLeds();
};

//--------------------------------------------------
//setzeStoerung
//Setzt den Status "Stoerung"
//Parameter:
//		pStoerung
//          (0) - keine Stoerung
//          (1) - Stoerung
//Rueckgabewert:
//		(indirekt) - Ledaenderung, Statusaenderung
//--------------------------------------------------
module.exports.setzeStoerung = setzeStoerung = function(pStoerung){
    stoerung = pStoerung;
    if(stoerung === 1){
        setzeFehler();
        setzeBereitschaft(0);
    }
    else{
        setzeBereitschaft(1);
    }
};


//--------------------------------------------------
//setzeScanner
//Setzt den Status "Scanner"
//Parameter:
//		pScanner
//          (0) - deaktiviert
//          (1) - aktiviert
//Rueckgabewert:
//		(indirekt) - Ledaenderung
//--------------------------------------------------
module.exports.setzeScanner = setzeScanner = function(pScanner){
    scanner = pScanner;
    //aktualisiereLeds();
};

//--------------------------------------------------
//setzeTuer
//Setzt den Zustand "Tuer"
//Parameter:
//		pTuer
//          (0) - deaktiviert
//          (1) - aktiviert
//Rueckgabewert:
//		(indirekt) - Ledaenderung
//--------------------------------------------------
module.exports.setzeTuer = setzeTuer = function(pTuer){
    console.log(config.tueroeffner);
    tuer = pTuer;
    led.oeffneTuere(1);
    aktualisiereLeds();
    setTimeout(() => {
        led.oeffneTuere(0);
        tuer = 0;
        aktualisiereLeds();
    }, config.tueroeffner);
};

//--------------------------------------------------
//setzeNutzungszeitCheckbox
//Aktiviert bzw deaktiviert die Einstellungen fuer die Nutzungsdauer
//Parameter:
//		pNutzungszeitCheckbox
//          (0) - deaktiviert
//          (1) - aktiviert
//Rueckgabewert:
//		(indirekt) - Statusaenderung
//--------------------------------------------------
module.exports.setzeNutzungszeitCheckbox = setzeNutzungszeitCheckbox = function(pNutzungszeitCheckbox){
    nutzungszeitCheckbox = pNutzungszeitCheckbox;
    aktualisiereNutzungszeit();
};

//--------------------------------------------------
//setzeNutzungszeitraum
//Setzt den Zeitbereich der Nutzungszeit
//Parameter:
//		pNutzungszeitBeginn - Startzeit
//      pNutzungszeitEnde - Endzeit
//Rueckgabewert:
//		(indirekt) - Statusaenderung
//--------------------------------------------------
module.exports.setzeNutzungszeitraum = setzeNutzungszeitraum = function(pNutzungszeitBeginn, pNutzungszeitEnde){
    nutzungszeitBeginn = pNutzungszeitBeginn;
    nutzungszeitEnde = pNutzungszeitEnde;
    aktualisiereNutzungszeit();
};

//--------------------------------------------------
//setzeBefuellroutine
//Setzt den Status "Befuellungsroutine"
//Parameter:
//		pBefuellroutine
//          (0) - inaktiv
//          (1) - aktiv
//Rueckgabewert:
//		(indirekt) - Ledaenderung
//--------------------------------------------------
module.exports.setzeBefuellroutine = setzeBefuellroutine = function(pBefuellroutine){
    befuellroutine = pBefuellroutine;
    led.setzeGrueneLed(0);
    aktualisiereLeds();
};

//--------------------------------------------------
//setzeFehler
//Setzt den Zustand der roten Led
//Parameter:
//		keinen
//Rueckgabewert:
//		(indirekt) - Ledaenderung
//--------------------------------------------------
module.exports.setzeFehler = setzeFehler = function(){
    if(led.setzeRoteLed(2) == true){
        aktualisiereLeds();
    }
};





//------------------------------------------------------------------------------------------------------------------------------------------------------
//	HOLEFUNKTIONEN
//------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------
//holeBereitschaft
//Gibt den Wert des Status "Bereitschaft" zurueck
//Parameter:
//		keinen
//Rueckgabewert:
//		(bereitschaft) - Wert der Bereitschaft
//--------------------------------------------------
module.exports.holeBereitschaft = holeBereitschaft = function(){
    return(bereitschaft);
};

//--------------------------------------------------
//holeFuellstand
//Gibt den Wert des Status "Fuellstand" zurueck
//Parameter:
//		keinen
//Rueckgabewert:
//		(fuellstand) - Wert der Bereitschaft
//--------------------------------------------------
module.exports.holeFuellstand = holeFuellstand = function(){
    return(fuellstand);
};

//--------------------------------------------------
//holeStoerung
//Gibt den Wert des Status "Stoerung" zurueck
//Parameter:
//		keinen
//Rueckgabewert:
//		(stoerung) - Wert der Bereitschaft
//--------------------------------------------------
module.exports.holeStoerung = holeStoerung = function(){
    return(stoerung);
};

//--------------------------------------------------
//holeScanner
//Gibt den Wert des Status "Scanner" zurueck
//Parameter:
//		keinen
//Rueckgabewert:
//		(scanner) - Wert der Bereitschaft
//--------------------------------------------------
module.exports.holeScanner = holeScanner = function(){
    return(scanner);
};

//--------------------------------------------------
//holeNutzungszeitraum
//Gibt alle Werte des Nutzungszeitraums zurueck
//Parameter:
//		keinen
//Rueckgabewert:
//		callback
//          nutzungszeitCheckbox - Aktivitaet der Nutzungszeit
//          nutzungszeitBeginn - Startwert
//          nutzungszeitEnde - Endwert
//--------------------------------------------------
module.exports.holeNutzungszeitraum = holeNutzungszeitraum = function(callback){
    callback(nutzungszeitCheckbox, nutzungszeitBeginn, nutzungszeitEnde);
};