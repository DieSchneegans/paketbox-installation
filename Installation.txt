#Skript zur Installation der DebianVM zum Softwareprojekt der Gruppe 33

#Autor: Marcel Schneegans (mar.schneegans.18@lehre.mosbach.dhbw.de)

ip a (IP-Adresse notieren!)

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git
git init
git clone https://gitlab.com/DieSchneegans/paketbox-installation.git
cd paketbox-installation
sudo chmod +x ./InstallServer.sh

sh InstallServer.sh