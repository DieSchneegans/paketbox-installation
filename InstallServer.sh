#!/bin/bash
#Skript zur Installation des Servers fuer Softwareprojekt der Gruppe 33
#Authoren: Marcel Schneegans (mar.schneegans.18@lehre.mosbach.dhbw.de), Felix Fakner (fel.fakner.18@lehre.mosbach.dhbw.de)

#Abkuerzungen
INSTALL="sudo apt-get install -y "
NPM="npm install "
UPDATE="sudo apt-get update"

#Zeitzone aendern
sudo ln -fs ~/usr/share/zoneinfo/Europe/Berlin /etc/localtime
sudo dpkg-reconfigure -f noninteractive tzdata

#System aktuallisieren
$UPDATE
sudo apt-get upgrade

#Curl
$INSTALL curl

#MYSQL Datenbank
$INSTALL mysql-server
sudo mysql -uroot <<MYSQL_SCRIPT
CREATE DATABASE Paketbox;

CREATE USER 'paketbox' IDENTIFIED BY 'softwareprojekt';
GRANT USAGE ON *.* TO 'paketbox'@localhost IDENTIFIED BY 'softwareprojekt';
GRANT USAGE ON *.* TO 'paketbox'@'%' IDENTIFIED BY 'softwareprojekt';
GRANT ALL privileges ON Paketbox.* TO 'paketbox'@localhost;
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'paketbox'@localhost; 

USE Paketbox;

SET time_zone = '+0:00';
SET GLOBAL time_zone = '+0:00';

CREATE TABLE Paketbox.Mailempfaenger(Name VARCHAR(255), Emailadresse VARCHAR(255));
CREATE TABLE Paketbox.Serverlog(Zeit DATETIME, Code VARCHAR(5), Text TEXT);
CREATE TABLE Paketbox.Empfangene_Pakete(Sendungsnummer INT PRIMARY KEY, Notiz TEXT, Zeit_eingetragen DATETIME, Zeit_zugestellt DATETIME);
CREATE TABLE Paketbox.Aktuelle_Pakete(Sendungsnummer INT PRIMARY KEY, Notiz TEXT, Zeit_eingetragen DATETIME);
MYSQL_SCRIPT

#Mosquitto
$INSTALL mosquitto
$INSTALL mosquitto-clients
$INSTALL libmosquitto-dev
$INSTALL libmosquittopp-dev

#Verbindung VS2019
$INSTALL openssh-server
$INSTALL g++
$INSTALL gcc
$INSTALL gdb
$INSTALL zip
$INSTALL gdbserver

#Nodejs
curl -sL https://deb.nodesource.com/setup_13.x | bash -
$UPDATE
$INSTALL nodejs
sudo curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
$UPDATE
$INSTALL nodejs
curl -L https://npmjs.org/install.sh | sudo sh

#Systempfad aendern
cd ..
mkdir softwareprojekt
cd softwareprojekt

mv /home/pi/paketbox-installation/Paketbox /home/pi/softwareprojekt/

mv /home/pi/paketbox-installation/module /home/pi/softwareprojekt/

mv /home/pi/paketbox-installation/Audio /home/pi/softwareprojekt/

mv /home/pi/paketbox-installation/Cpp /home/pi/softwareprojekt/

mv /home/pi/paketbox-installation/StartServer.sh /home/pi/softwareprojekt/

mv /home/pi/paketbox-installation/package.json /home/pi/softwareprojekt/

mv /home/pi/paketbox-installation/package-lock.json /home/pi/softwareprojekt/

sudo mv /home/pi/paketbox-installation/Autostart/server.service /lib/systemd/system/

sudo mv /home/pi/paketbox-installation/Autostart/hardware.service /lib/systemd/system/

sudo chmod 644 /lib/systemd/system/server.service

sudo chmod 644 /lib/systemd/system/hardware.service

#NPM Erweiterungen
sudo npm cache clean -f
sudo npm install -g n
sudo n stable

$NPM mysql --save
$NPM ws --save
$NPM onoff --save
$NPM socket.io --save
$NPM nodemailer --save
$NPM express --save
$NPM mqtt --save

sudo systemctl daemon-reload

sudo systemctl enable server.service

sudo systemctl enable hardware.service

sudo reboot